﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class BlockMesh : MonoBehaviour
{

    public MeshFilter _filter;
    Mesh _mesh;
    public int width = 16;
    public int height = 16;
    int[] _triangles;
    Vector3[] _vertices;
    Vector3[] _normals;
    Vector2[] _uvs;

    Vector3 ScaleVec(Vector3 vec, Vector3 scale)
    {
        return new Vector3(vec.x * scale.x, vec.y * scale.y, vec.z * scale.z);
    }

    void SetScale(int x, int y, Vector3 scale)
    {
        //Debug.Log("BlockMesh: SetScale");
        int vOffset = (y * width + x) * (6 * 4);
        Vector3 cubeOrigin = new Vector3(x, y, 0);
        // top
        _vertices[vOffset + 0] = ScaleVec(new Vector3(-.5f, +.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 1] = ScaleVec(new Vector3(-.5f, +.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 2] = ScaleVec(new Vector3(+.5f, +.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 3] = ScaleVec(new Vector3(+.5f, +.5f, +.5f), scale) + cubeOrigin;
        vOffset += 4;
        // bottom
        _vertices[vOffset + 0] = ScaleVec(new Vector3(-.5f, -.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 1] = ScaleVec(new Vector3(-.5f, -.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 2] = ScaleVec(new Vector3(+.5f, -.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 3] = ScaleVec(new Vector3(+.5f, -.5f, +.5f), scale) + cubeOrigin;
        vOffset += 4;
        // left
        _vertices[vOffset + 0] = ScaleVec(new Vector3(-.5f, -.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 1] = ScaleVec(new Vector3(-.5f, -.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 2] = ScaleVec(new Vector3(-.5f, +.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 3] = ScaleVec(new Vector3(-.5f, +.5f, +.5f), scale) + cubeOrigin;
        vOffset += 4;
        // right
        _vertices[vOffset + 0] = ScaleVec(new Vector3(+.5f, -.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 1] = ScaleVec(new Vector3(+.5f, -.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 2] = ScaleVec(new Vector3(+.5f, +.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 3] = ScaleVec(new Vector3(+.5f, +.5f, +.5f), scale) + cubeOrigin;
        vOffset += 4;
        // front
        _vertices[vOffset + 0] = ScaleVec(new Vector3(-.5f, -.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 1] = ScaleVec(new Vector3(-.5f, +.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 2] = ScaleVec(new Vector3(+.5f, -.5f, -.5f), scale) + cubeOrigin;
        _vertices[vOffset + 3] = ScaleVec(new Vector3(+.5f, +.5f, -.5f), scale) + cubeOrigin;
        vOffset += 4;
        // back
        _vertices[vOffset + 0] = ScaleVec(new Vector3(-.5f, -.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 1] = ScaleVec(new Vector3(-.5f, +.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 2] = ScaleVec(new Vector3(+.5f, -.5f, +.5f), scale) + cubeOrigin;
        _vertices[vOffset + 3] = ScaleVec(new Vector3(+.5f, +.5f, +.5f), scale) + cubeOrigin;
        vOffset += 4;
    }

    void Update()
    {

    }

    void Start()
    {
        Debug.Log("BlockMesh: Refresh");
        // Make the mesh
        _mesh = new Mesh();

        int[] triangles = new int[width * height * 6 * 2 * 3];
        Vector3[] vertices = new Vector3[width * height * 6 * 4];
        Vector3[] normals = new Vector3[width * height * 6 * 4];
        Vector2[] uvs = new Vector2[width * height * 6 * 4];

        Debug.Log(vertices.Length);
        int vOffset = 0;
        int triOffset = 0;
        Vector3 cubeOrigin = Vector3.zero;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                cubeOrigin = new Vector3(x, y, 0);
                // top
                vertices[vOffset + 0] = new Vector3(-.5f, +.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 1] = new Vector3(-.5f, +.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 2] = new Vector3(+.5f, +.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 3] = new Vector3(+.5f, +.5f, +.5f) + cubeOrigin;
                normals[vOffset + 0] = Vector3.up;
                normals[vOffset + 1] = Vector3.up;
                normals[vOffset + 2] = Vector3.up;
                normals[vOffset + 3] = Vector3.up;
                uvs[vOffset + 0] = Vector2.zero;
                uvs[vOffset + 1] = Vector2.up;
                uvs[vOffset + 2] = Vector2.right;
                uvs[vOffset + 3] = Vector2.one;
                triangles[triOffset + 0] = vOffset + 0;
                triangles[triOffset + 1] = vOffset + 1;
                triangles[triOffset + 2] = vOffset + 3;
                triangles[triOffset + 3] = vOffset + 3;
                triangles[triOffset + 4] = vOffset + 2;
                triangles[triOffset + 5] = vOffset + 0;
                vOffset += 4;
                triOffset += 6;
                // bottom
                vertices[vOffset + 0] = new Vector3(-.5f, -.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 1] = new Vector3(-.5f, -.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 2] = new Vector3(+.5f, -.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 3] = new Vector3(+.5f, -.5f, +.5f) + cubeOrigin;
                normals[vOffset + 0] = Vector3.down;
                normals[vOffset + 1] = Vector3.down;
                normals[vOffset + 2] = Vector3.down;
                normals[vOffset + 3] = Vector3.down;
                uvs[vOffset + 0] = Vector2.one;
                uvs[vOffset + 1] = Vector2.right;
                uvs[vOffset + 2] = Vector2.up;
                uvs[vOffset + 3] = Vector2.zero;
                triangles[triOffset + 0] = vOffset + 0;
                triangles[triOffset + 1] = vOffset + 2;
                triangles[triOffset + 2] = vOffset + 3;
                triangles[triOffset + 3] = vOffset + 3;
                triangles[triOffset + 4] = vOffset + 1;
                triangles[triOffset + 5] = vOffset + 0;
                vOffset += 4;
                triOffset += 6;
                // left
                vertices[vOffset + 0] = new Vector3(-.5f, -.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 1] = new Vector3(-.5f, -.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 2] = new Vector3(-.5f, +.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 3] = new Vector3(-.5f, +.5f, +.5f) + cubeOrigin;
                normals[vOffset + 0] = Vector3.left;
                normals[vOffset + 1] = Vector3.left;
                normals[vOffset + 2] = Vector3.left;
                normals[vOffset + 3] = Vector3.left;
                uvs[vOffset + 0] = Vector2.zero;
                uvs[vOffset + 1] = Vector2.up;
                uvs[vOffset + 2] = Vector2.right;
                uvs[vOffset + 3] = Vector2.one;
                triangles[triOffset + 0] = vOffset + 0;
                triangles[triOffset + 1] = vOffset + 1;
                triangles[triOffset + 2] = vOffset + 3;
                triangles[triOffset + 3] = vOffset + 3;
                triangles[triOffset + 4] = vOffset + 2;
                triangles[triOffset + 5] = vOffset + 0;
                vOffset += 4;
                triOffset += 6;
                // right
                vertices[vOffset + 0] = new Vector3(+.5f, -.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 1] = new Vector3(+.5f, -.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 2] = new Vector3(+.5f, +.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 3] = new Vector3(+.5f, +.5f, +.5f) + cubeOrigin;
                normals[vOffset + 0] = Vector3.right;
                normals[vOffset + 1] = Vector3.right;
                normals[vOffset + 2] = Vector3.right;
                normals[vOffset + 3] = Vector3.right;
                uvs[vOffset + 0] = Vector2.one;
                uvs[vOffset + 1] = Vector2.right;
                uvs[vOffset + 2] = Vector2.up;
                uvs[vOffset + 3] = Vector2.zero;
                triangles[triOffset + 0] = vOffset + 0;
                triangles[triOffset + 1] = vOffset + 2;
                triangles[triOffset + 2] = vOffset + 3;
                triangles[triOffset + 3] = vOffset + 3;
                triangles[triOffset + 4] = vOffset + 1;
                triangles[triOffset + 5] = vOffset + 0;
                vOffset += 4;
                triOffset += 6;
                // front
                vertices[vOffset + 0] = new Vector3(-.5f, -.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 1] = new Vector3(-.5f, +.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 2] = new Vector3(+.5f, -.5f, -.5f) + cubeOrigin;
                vertices[vOffset + 3] = new Vector3(+.5f, +.5f, -.5f) + cubeOrigin;
                normals[vOffset + 0] = Vector3.forward;
                normals[vOffset + 1] = Vector3.forward;
                normals[vOffset + 2] = Vector3.forward;
                normals[vOffset + 3] = Vector3.forward;
                uvs[vOffset + 0] = Vector2.zero;
                uvs[vOffset + 1] = Vector2.up;
                uvs[vOffset + 2] = Vector2.right;
                uvs[vOffset + 3] = Vector2.one;
                triangles[triOffset + 0] = vOffset + 0;
                triangles[triOffset + 1] = vOffset + 1;
                triangles[triOffset + 2] = vOffset + 3;
                triangles[triOffset + 3] = vOffset + 3;
                triangles[triOffset + 4] = vOffset + 2;
                triangles[triOffset + 5] = vOffset + 0;
                vOffset += 4;
                triOffset += 6;
                // back
                vertices[vOffset + 0] = new Vector3(-.5f, -.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 1] = new Vector3(-.5f, +.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 2] = new Vector3(+.5f, -.5f, +.5f) + cubeOrigin;
                vertices[vOffset + 3] = new Vector3(+.5f, +.5f, +.5f) + cubeOrigin;
                normals[vOffset + 0] = Vector3.back;
                normals[vOffset + 1] = Vector3.back;
                normals[vOffset + 2] = Vector3.back;
                normals[vOffset + 3] = Vector3.back;
                uvs[vOffset + 0] = Vector2.one;
                uvs[vOffset + 1] = Vector2.right;
                uvs[vOffset + 2] = Vector2.up;
                uvs[vOffset + 3] = Vector2.zero;
                triangles[triOffset + 0] = vOffset + 0;
                triangles[triOffset + 1] = vOffset + 2;
                triangles[triOffset + 2] = vOffset + 3;
                triangles[triOffset + 3] = vOffset + 3;
                triangles[triOffset + 4] = vOffset + 1;
                triangles[triOffset + 5] = vOffset + 0;
                vOffset += 4;
                triOffset += 6;
            }
        }

        _mesh.SetVertices(new List<Vector3>(vertices));
        _mesh.SetTriangles(triangles, 0);
        _mesh.SetUVs(0, new List<Vector2>(uvs));
        _mesh.SetNormals(new List<Vector3>(normals));

        _vertices = _mesh.vertices;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                SetScale(x, y, new Vector3(1f, 1f, Random.Range(0.5f, 10f)));
            }
        }

        _mesh.vertices = _vertices;
        _mesh.RecalculateBounds();

        _filter.mesh = _mesh;
    }

    public WebCamTexture _Data;
    public float depth = 10.0f;
    public int subsampling = 4;

    void LateUpdate()
    {
        _mesh = _filter.mesh;
        _vertices = _mesh.vertices;
        if (_Data != null)
        {
            //Debug.Log("Update");
            int dataWidth = _Data.width;
            int dataHeight = _Data.height;
            float displacement = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    displacement = _Data.GetPixel(dataWidth / width * x, dataHeight / height * y).grayscale;
                    if (displacement < 0.1f)
                        SetScale(x, y, new Vector3(0, 0, displacement * depth));
                    else
                        SetScale(x, y, new Vector3(.5f, .5f, displacement * depth));
                }
            }
        }
        _mesh.vertices = _vertices;
        _filter.mesh = _mesh;
    }

}
