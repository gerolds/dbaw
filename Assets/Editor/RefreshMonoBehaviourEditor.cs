﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(RefreshMonoBehaviour), true)]
public class RefreshMonoBehaviourEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		RefreshMonoBehaviour script = (RefreshMonoBehaviour)target;
		if(GUILayout.Button("Refresh Object"))
		{
			script.SendMessage("OnEditorRefresh", SendMessageOptions.RequireReceiver);
		}
	}
}