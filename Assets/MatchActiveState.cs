﻿using UnityEngine;
using System.Collections;

public class MatchActiveState : MonoBehaviour
{

	public GameObject _TargetToMatch;

	void Update ()
	{
		foreach (Transform child in transform) {
			child.gameObject.SetActive (_TargetToMatch.activeInHierarchy);
		}
	}
}
