﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;

public enum MirrorAxis
{
	X,
	Y,
	Z
}

public class Mirror : MonoBehaviour
{
	private Vector3 _originalScale;
	private Vector3 _originalPosition;
	private Quaternion _originalRotation;

	public bool _MirrorPosition = true;
	public bool _MirrorScale = true;
	public MirrorAxis _MirrorAxis = MirrorAxis.Y;
	public float _deadZone = 0.25f;
	private bool _isActive = false;
	Vector3 _delta = Vector3.zero;

	void Start ()
	{
		_originalScale = this.transform.localScale;
		_originalPosition = this.transform.localPosition;
		_originalRotation = this.transform.rotation;

	}

	void Update ()
	{
		int x = 1;
		int y = 1;
		int z = 1;

		x = _MirrorAxis == MirrorAxis.X ? -1 : 1;
		y = _MirrorAxis == MirrorAxis.Y ? -1 : 1;
		z = _MirrorAxis == MirrorAxis.Z ? -1 : 1;

		_delta = transform.position - Camera.main.transform.position;

		_isActive = (_MirrorAxis == MirrorAxis.X && Mathf.Abs (_delta.x) > _deadZone) ? _delta.x > 0 : _isActive;
		_isActive = (_MirrorAxis == MirrorAxis.Y && Mathf.Abs (_delta.y) > _deadZone) ? _delta.y > 0 : _isActive;
		_isActive = (_MirrorAxis == MirrorAxis.Z && Mathf.Abs (_delta.z) > _deadZone) ? _delta.z > 0 : _isActive;

		if (_isActive) {
			if (_MirrorPosition) {
				this.transform.localPosition = new Vector3 (
					_originalPosition.x * x, 
					_originalPosition.y * y,
					_originalPosition.z * z
				);
			}
			if (_MirrorScale) {
				this.transform.localScale = new Vector3 (
					_originalScale.x * x, 
					_originalScale.y * y,
					_originalScale.z * z
				);
			}
		} else {
			this.transform.localScale = _originalScale;
			this.transform.localPosition = _originalPosition;
		}
	}
}
