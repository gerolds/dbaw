﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour
{

    Vector3 _originalPos;
    Quaternion _originalRotation;
    float _duration = 30f;
    float _radius;

    void Start()
    {
        _originalPos = transform.position;
        _radius = new Vector2(_originalPos.x, _originalPos.z).magnitude;
    }

    void Update()
    {
        if (GameManager.singleton.CurrentState != GState.Ready)
        {
            transform.position = _originalPos;
            transform.rotation = _originalRotation;
        }

        transform.position = new Vector3(
            Mathf.Sin(Time.time / 30f * Mathf.PI),
            _originalPos.y,
            Mathf.Cos(Time.time / 30f * Mathf.PI)
        );

        Vector3 fwd = new Vector3(
                          -transform.position.x,
                          0,
                          -transform.position.z
                      );
        transform.rotation = Quaternion.LookRotation(fwd, Vector3.up);
    }
}
