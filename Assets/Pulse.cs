﻿using UnityEngine;
using System.Collections;

public class Pulse : MonoBehaviour
{

    public GameObject _target;
    public float _frequency = 0.5f;
    public float _amplitude = 0.25f;
    Vector3 _originalScale;

    void Start()
    {
        _originalScale = _target.transform.localScale;    
    }

    void Update()
    {
        _target.transform.localScale = _originalScale * (1f + _amplitude * Mathf.Sin(Time.time * _frequency * Mathf.PI));  
    }
}
