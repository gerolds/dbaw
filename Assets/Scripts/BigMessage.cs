﻿using UnityEngine;
using System.Collections;

public class BigMessage : MonoBehaviour
{

	#region SINGLETON

	public static BigMessage singleton = null;
	public float _Duration = 1.0f;

	void Awake ()
	{
		// Singleton
		if (singleton == null) {
			singleton = this;
		} else if (singleton != this) {
			Destroy (gameObject);    
		}
	}

	#endregion

	public UnityEngine.UI.Text _Target;
	private Vector3 origScale;

	void Start ()
	{
		if (_Target == null) {
			Debug.LogWarning ("BigMessage target missing");
		}
		origScale = _Target.rectTransform.localScale;
		_Target.color = new Color (
			_Target.color.r,
			_Target.color.g,
			_Target.color.b,
			0
		);
	}

	public void ShowMessage (string msg)
	{
		Color visible = new Color (
			_Target.color.r,
			_Target.color.g,
			_Target.color.b,
			1.0f
		);
		_Target.text = msg;

		Color invisible = new Color (
			                     _Target.color.r,
			                     _Target.color.g,
			                     _Target.color.b,
			                     0
		                     );
		_Target.rectTransform.localScale = origScale;
		LeanTween.cancel (gameObject);
		LeanTween.textColor (_Target.rectTransform, visible, 0.1f);
		LeanTween.scale (_Target.rectTransform, _Target.rectTransform.localScale * 1.2f, 0.1f);
		LeanTween.textColor (_Target.rectTransform, invisible, _Duration - 0.4f).setDelay (0.4f);
		LeanTween.scale (_Target.rectTransform, _Target.rectTransform.localScale * 0.8f, _Duration).setDelay (0.1f);
	}
}
