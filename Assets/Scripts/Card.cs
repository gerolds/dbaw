﻿using UnityEngine;
using System.Collections;

public enum CardElement {
	A,
	B,
	C
}

public class Card : MonoBehaviour {

	public int Strength = 1;
	public CardElement Element = CardElement.A;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
