﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CityFoundation : MonoBehaviour
{
    public GameObject _BuildingPrefab;

    public float _Area = 16.0f;
    public int _Division = 7;
    public float _minXZScale = 1.0f;
    //public float _maxXZScale = 2.0f;
    public float _minYScale = 0.5f;
    public float _maxYScale = 4.0f;
    [Range(0.0f, 4.0f)]
    public float _gradientEffect = 0.5f;
    //public float _Spacing;
    private CityTile[] tiles;

    void Start()
    {
        SpawnCity();
    }

    // Use this for initialization
    void SpawnCity()
    {
        Random.InitState(16);
        Debug.Log("Rebuilding Cityscape");
        var childObjects = transform.Cast<Transform>().ToList();
        if (Application.isEditor) {
            foreach (Transform child in childObjects) {
                DestroyImmediate(child.gameObject);
            }
        } else {
            foreach (Transform child in childObjects) {
                Destroy(child.gameObject);
            }
        }

        if (_Division <= 0) {
            _Division = 1;
        }
        tiles = new CityTile[_Division * _Division];
        float spacing = _Area / (float)_Division;
        GameObject newObj;
        float xzScale = 0;
        float yScale = 0;
        Vector3 offset = transform.position + new Vector3(-_Area / 2.0f + spacing / 2.0f, 0, -_Area / 2.0f + spacing / 2.0f);
        for (int y = 0; y < _Division; y++) {
            for (int x = 0; x < _Division; x++) {
                float modifier = Mathf.Pow(1f - (Vector2.Distance(new Vector2((float)_Division / 2f, (float)_Division / 2f), new Vector2((float)x, (float)y)) / (float)_Division), 2f) * _gradientEffect + 1f;
                xzScale = Random.Range(spacing * _minXZScale, spacing);
                yScale = Mathf.Pow(Random.Range(_minYScale, _maxYScale) * modifier, 2f) * spacing;
                newObj = (GameObject)Instantiate(_BuildingPrefab, offset + new Vector3((float)x * spacing, 0, (float)y * spacing), Quaternion.identity, this.transform);
                newObj.transform.localScale = new Vector3(xzScale, yScale, xzScale);
                newObj.transform.position += new Vector3(0.25f, 0, 0.25f) * Random.Range(-spacing + newObj.transform.localScale.x, spacing - newObj.transform.localScale.x);
                CityTile newTile = newObj.GetComponent<CityTile>();
                tiles[y * _Division + x] = newTile;
                newTile.Refresh();
            }
        }
    }

    public void UnlockTile(int x, int y)
    {
        tiles[y * _Division + x].Grow();
    }

    public void LockTile(int x, int y)
    {
        tiles[y * _Division + x].Shrink();
    }

    protected void Refresh()
    {
        SpawnCity();
    }
}
