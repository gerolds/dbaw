﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class CityTile : MonoBehaviour
{
	public GameObject _BlockPrefab;

	//public float _Distance = 0.5f;
	public float _Area = 2.0f;
	[Range (1, 8)]
	public int _Divisions = 4;
	private float _AreaScale = 1.0f;
	[Range (1f, 4f)]
	public float _AreaRange = 2.0f;
	[Range (.1f, 4f)]
	public float _BaseHeight = 1.0f;
	[Range (0.1f, 2.0f)]
	public float _BaseHeightScale = 1.0f;
	[Range (1f, 4f)]
	public float _HeightRange = 2.0f;
	[Range (1f, 4f)]
	public float _Contrast = 2.0f;

	public Color BlockColor;

	private float _minXZ;
	private float _maxXZ;
	private float _spacing;

	public void Start ()
	{
		//Debug.Log("Calling Refresh");
		Refresh ();
	}

	public void Refresh ()
	{
		//Debug.Log("Refresh Called");
		if (_Divisions <= 0) {
			_Divisions = 1;
		}

		var childObjects = transform.Cast<Transform> ().ToList ();
		if (Application.isEditor) {
			foreach (Transform child in childObjects) {
				DestroyImmediate (child.gameObject);
			}
		} else {
			foreach (Transform child in childObjects) {
				Destroy (child.gameObject);
			}
		}

		_spacing = _Area / (float)_Divisions;
		_minXZ = _spacing * _AreaScale;
		_maxXZ = _spacing * _AreaScale * _AreaRange;

		//Debug.Log(_spacing);
		GameObject newObj;
		float xzScale = 0;
		float yScale = 0;
		Vector3 offset = new Vector3 (-_Area / 2.0f + _spacing / 2.0f, 0, -_Area / 2.0f + _spacing / 2.0f);
		for (int y = 0; y < _Divisions; y++) {
			for (int x = 0; x < _Divisions; x++) {
//                float modifier = Mathf.Pow(
//                                     1f - (Vector2.Distance(
//                                         new Vector2((float)_Divisions / 2f, (float)_Divisions / 2f), 
//                                         new Vector2((float)x, (float)y)
//                                     ) / (float)_Divisions), _Contrast) * 0.5f + 0.25f;
				xzScale = Random.Range (_minXZ, _maxXZ);
				yScale = (Mathf.Pow (Random.Range (_BaseHeightScale, _BaseHeightScale * _HeightRange), 2f) + _BaseHeight) * _spacing;
				newObj = (GameObject)Instantiate (
					original: _BlockPrefab, 
					position: offset + new Vector3 ((float)x * _spacing, 0, (float)y * _spacing), 
					rotation: Quaternion.identity, 
					parent: this.transform
				);
				Vector3 targetScale = new Vector3 (xzScale, yScale, xzScale);
				if (Application.isPlaying) {
					newObj.transform.localScale = Vector3.zero;
					//LeanTween.scale(newObj, targetScale * 0.8f, yScale * 2f).setDelay(Random.Range(0.5f, 5.0f));
				} else {
					newObj.transform.localScale = targetScale;
				}
				//newObj.transform.localPosition = new Vector3(1f, 0, 1f) * Random.Range(-_maxXZ + newObj.transform.localScale.x, _maxXZ - newObj.transform.localScale.x);
				newObj.transform.localPosition = offset + new Vector3 ((float)x * _spacing, 0, (float)y * _spacing);
				newObj.GetComponent<Renderer> ().material.color = BlockColor;
				newObj.GetComponent<TransformSnapshot> ().scale = targetScale;
			}
		}
	}

	public void Grow ()
	{
		var childObjects = transform.Cast<Transform> ().ToList ();
		foreach (Transform child in childObjects) {
			LeanTween.scale (child.gameObject, child.GetComponent<TransformSnapshot> ().scale, 0.5f).setDelay (Random.Range (0.0f, 0.5f));
		}        
	}

	public void Shrink ()
	{
		Shrink (0.5f, 5f, 5f);
	}

	public void Shrink (float s, float baseDuration, float delayRange)
	{
		var childObjects = transform.Cast<Transform> ().ToList ();
		TransformSnapshot snapshot;
		foreach (Transform child in childObjects) {
			LeanTween.cancel (child.gameObject);
			snapshot = child.GetComponent<TransformSnapshot> ();
			LeanTween.scale (child.gameObject, snapshot.scale * s, snapshot.scale.y * baseDuration).setDelay (Random.Range (0.0f, delayRange));
		}
	}
}
