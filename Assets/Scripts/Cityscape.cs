﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class Cityscape : RefreshMonoBehaviour
{
    public GameObject _BuildingPrefab;

    public float _Area = 16.0f;
    public int _Division = 7;
    public float _minXZScale = 1.0f;
    public float _maxXZScale = 2.0f;
    public float _minYScale = 0.5f;
    public float _maxYScale = 4.0f;
    //public float _Spacing;

    void Start()
    {
        SpawnCity();
    }

    // Use this for initialization
    void SpawnCity()
    {
        Random.InitState(16);
        Debug.Log("Rebuilding Cityscape");
        var childObjects = transform.Cast<Transform>().ToList();
        if (Application.isEditor)
        {
            foreach (Transform child in childObjects)
            {
                DestroyImmediate(child.gameObject);
            }
        }
        else
        {
            foreach (Transform child in childObjects)
            {
                Destroy(child.gameObject);
            }
        }

        if (_Division <= 0)
        {
            _Division = 1;
        }
        float spacing = _Area / (float)_Division;
        GameObject newObj;
        float xzScale = 0;
        float yScale = 0;
        Vector3 scale = Vector3.zero;
        Vector3 offset = transform.position + new Vector3(-_Area / 2.0f + spacing / 2.0f, 0, -_Area / 2.0f + spacing / 2.0f);
        for (int y = 0; y < _Division; y++)
        {
            for (int x = 0; x < _Division; x++)
            {
                float modifier = Mathf.Pow(1f - (Vector2.Distance(new Vector2((float)_Division / 2f, (float)_Division / 2f), new Vector2((float)x, (float)y)) / (float)_Division), 2f) * 0.5f + 0.25f;
                xzScale = Random.Range(_minXZScale, _maxXZScale);
                yScale = Mathf.Pow(Random.Range(_minYScale, _maxYScale) * modifier, 2f);
                scale = new Vector3(xzScale, yScale, xzScale);
                newObj = (GameObject)Instantiate(_BuildingPrefab, offset + new Vector3((float)x * spacing, 0, (float)y * spacing), Quaternion.identity, this.transform);
                //newObj.transform.localScale = new Vector3(xzScale, yScale, xzScale);
                newObj.transform.position += new Vector3(0.25f, 0, 0.25f) * Random.Range(-_maxXZScale + newObj.transform.localScale.x, _maxXZScale - newObj.transform.localScale.x);
                LeanTween.scale(newObj, scale, yScale * 0.5f);
            }
        }


    }

    override protected void OnEditorRefresh()
    {
        SpawnCity();
    }
}
