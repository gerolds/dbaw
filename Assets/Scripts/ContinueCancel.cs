﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ContinueCancel : MonoBehaviour
{
    public KeyCode ContinueKey = KeyCode.Space;
    public KeyCode CancelKey = KeyCode.Escape;
    public Text Target;
    public string ContinueText = "Continue";
    public string CancelText = "Cancel";

    void Start()
    {
        if (Target != null)
            Target.text = string.Format(" [{0}] {2}, [{1}] {3}", ContinueKey, CancelKey, ContinueText, CancelText);
    }

    void Update()
    {
        if (Input.GetKeyDown(ContinueKey)) {
            Debug.Log("Manual Continue Signal");
            GameManager.singleton.ProcessEvent(GEvent.Continue);
        } else if (Input.GetKeyDown(CancelKey)) {
            Debug.Log("Manual Cancel Signal");
            GameManager.singleton.ProcessEvent(GEvent.Cancel);
        }

    }
}
