﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkManager
{

    public NetworkError _Error;
    public Text _StatusTarget;
    public Text _ObjectlistTarget;

    //    private List<Player> _players = new List<Player>();

    void OnEnable()
    {
        GameManager.singleton.OnEnterState.AddListener(EnterStateHandler);
        GameManager.singleton.OnExitState.AddListener(ExitStateHandler);
        //GameManager.singleton.OnUpdateState.RemoveListener(UpdateStateHandler);
    }

    void OnDisable()
    {
        GameManager.singleton.OnEnterState.RemoveListener(EnterStateHandler);
        GameManager.singleton.OnExitState.RemoveListener(ExitStateHandler);
        //GameManager.singleton.OnUpdateState.RemoveListener(UpdateStateHandler);
    }

    void EnterStateHandler(GState s)
    {
        switch (s)
        {
            case GState.Offline:
                StopHost();
                StopClient();
                StopServer();
                break;
            default:
                break;
        }
    }

    void ExitStateHandler(GState s)
    {
        
    }

    void UpdateStateHandler(GState s)
    {
        
    }

    void Update()
    {
        UpdateStatus();
    }

    void UpdateStatus()
    {
        _StatusTarget.text = "Player Count: " + NetworkManager.singleton.numPlayers;
        _StatusTarget.text += "\nConnections: " + Network.connections.Length;
        foreach (NetworkPlayer player in Network.connections)
        {
            _StatusTarget.text += "\nPlayer: " + player.ipAddress + " ";
        }
        _ObjectlistTarget.text = "(" + ClientScene.objects.Count + ")";

        foreach (var it in ClientScene.objects)
        {
            _ObjectlistTarget.text += ", " + it.Key;
        }
    
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);

        // The client lost its server-connection
        //Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
        Debug.Log("NetworkManager: Client disconnect");
        GameManager.singleton.ProcessEvent(GEvent.Disconnect);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);

        // The client established a server-connection
        //Debug.Log(SlotManager.singleton);
        //Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
        Debug.Log("NetworkManager: Client connect");
        GameManager.singleton.ProcessEvent(GEvent.Connect);
    }

    public override void OnStopClient()
    {
        //Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
        Debug.Log("NetworkManager: Client stop");
        GameManager.singleton.ProcessEvent(GEvent.Disconnect);
    }

    public override void OnStopServer()
    {
        //Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
        Debug.Log("NetworkManager: Sever stop");
        GameManager.singleton.ProcessEvent(GEvent.Disconnect);
    }

    //    public override void OnClientConnect(NetworkConnection conn) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnClientDisconnect(NetworkConnection conn ) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnClientNotReady(NetworkConnection conn) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnClientSceneChanged(NetworkConnection conn) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnDropConnection(bool success, string extendedInfo) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnServerConnect(NetworkConnection conn) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnServerDisconnect (NetworkConnection conn) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnServerReady (NetworkConnection conn) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnServerSceneChanged (string sceneName) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnStartClient (NetworkClient client) {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnStartHost() {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnStartServer() {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}
    //    public override void OnStopServer() {Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);}

    public void Stop()
    {
        GameManager.singleton.ProcessEvent(GEvent.Disconnect);
    }

    public void EngageHost()
    {
        StartHost();
    }

    public void EngageClient()
    {
        StartClient();
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
        _Error = (NetworkError)errorCode;
        Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
    }

    public override void OnServerError(NetworkConnection conn, int errorCode)
    {
        _Error = (NetworkError)errorCode;
        Debug.Log(System.Reflection.MethodBase.GetCurrentMethod().Name);
    }
}
