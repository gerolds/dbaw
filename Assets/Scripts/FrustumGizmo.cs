﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FrustumGizmo : MonoBehaviour
{

    public Vector3 Offset = Vector3.zero;
    public float FOV = 70.0f;
    public float MaxRange = 2.0f;
    public float MinRange = 0.5f;
    public float Aspect = 0.625f;

    void OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawFrustum(transform.position + Offset, FOV, MaxRange, MinRange, Aspect);
    }
}
