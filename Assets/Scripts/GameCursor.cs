﻿using UnityEngine;
using System.Collections;

public class GameCursor : MonoBehaviour
{

	public Camera cam;
	public LayerMask mask;
	private Vector2 mousePrev;
	float cursorYPos = 0;
	public float sensitivity = 5f;
	RaycastHit hit;
	public GameObject gizmo;
	public Vector3 gizmoDefaultScale = new Vector3 (0.5f, 0.5f, 0.5f);
	public Vector3 gizmoTriggerScale = new Vector3 (1.0f, 0.5f, 1.0f);
	public float triggerDecay = 5f;
	private bool _flip = false;
	public bool _devMode = true;

	void Update ()
	{

		if (!_devMode) {

			_flip = GameManager.singleton.CameraRig.transform.position.y < transform.position.y;

			if (Physics.Raycast (cam.transform.position, cam.transform.forward, out hit, 50, mask, QueryTriggerInteraction.Collide)) {
				cursorYPos = hit.point.y;
				//Debug.Log(hit.rigidbody.gameObject.name);
			}

			transform.position = new Vector3 (
				transform.position.x,
				cursorYPos,
				transform.position.z
			);

			transform.rotation = Quaternion.LookRotation (cam.transform.right - (Vector3.Dot (cam.transform.right, Vector3.up) * Vector3.up), Vector3.up);
			transform.eulerAngles = new Vector3 (0, transform.rotation.eulerAngles.y + 90, 0);

			transform.position += transform.right * Input.GetAxis ("Mouse X") * sensitivity * -0.001f;
			if (_flip)
				transform.position += transform.forward * Input.GetAxis ("Mouse Y") * -1f * sensitivity * -0.001f;
			else
				transform.position += transform.forward * Input.GetAxis ("Mouse Y") * sensitivity * -0.001f;
		} else {
			Ray r = cam.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (r, out hit, 500, mask, QueryTriggerInteraction.Collide)) {
				gizmo.transform.position = hit.point;
			}
		}

		if (Input.GetMouseButton (0))
			gizmo.transform.localScale = gizmoTriggerScale;

		gizmo.transform.localScale = Vector3.Lerp (gizmo.transform.localScale, gizmoDefaultScale, Time.deltaTime * triggerDecay);

	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawLine (transform.position, cam.transform.position);
		Gizmos.color = Color.green;
		Gizmos.DrawRay (cam.transform.position, cam.transform.forward * 5f);
	}

}
