﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using System;
using UnityEngine;

//using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

//using Valve.VR;
//#define LOG

public enum PlayerRole
{
    None = -1,
    Umbra = 0,
    Lux = 1
}

[Flags]
public enum GState
{
    Init = 1 << 0,
    Offline = 1 << 1,
    Running = 1 << 2,
    Paused = 1 << 3,
    Complete = 1 << 4,
    Intermission = 1 << 5,
    Error = 1 << 6,
    Help = 1 << 7,
    Online = 1 << 8,
    Reset = 1 << 9,
    Ready = 1 << 10
}

[Flags]
public enum GEvent
{
    Complete = 1 << 1,

    None = 1 << 2,
    Play = 1 << 3,
    Pause = 1 << 4,
    Reset = 1 << 5,
    Win = 1 << 6,
    Loose = 1 << 7,
    Error = 1 << 8,
    Connect = 1 << 9,
    Disconnect = 1 << 10,
    Exit = 1 << 11,
    Continue = 1 << 12,
    Cancel = 1 << 13
}

[System.Serializable]
public class UnityEventGEvent : UnityEvent<GEvent>
{
}

[System.Serializable]
public class UnityEventGState : UnityEvent<GState>
{
}

[System.Serializable]
public class UnityEventInt : UnityEvent<int>
{
}

public class GameManager : MonoBehaviour
{
    #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
    public const bool VR_ENABLED = false;
	















#elif UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
    public const bool VR_ENABLED = true;
    #endif

    public UnityEventGState OnEnterState = new UnityEventGState();
    public UnityEventGState OnExitState = new UnityEventGState();
    public UnityEventGState OnUpdateState = new UnityEventGState();

    #region Variables

    public int TargetFPS = 30;
    public Transform OfflineCameraPositionFar;
    public Transform OfflineCameraPosition;
    public Transform ReadyCameraPosition;
    public GameObject CameraRig;

    public UnityEngine.UI.Text _errorTextTarget;

    //public NetworkManager _NetworkManager;
    private GState _currentState = GState.Init;
    private GState _previousState = GState.Init;
    private GEvent _ev;
    private string _evComment;

    #endregion

    #region Singleton

    public static GameManager singleton = null;

    private void Awake()
    {
        // Singleton
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);    
        }
        Debug.Log("GameManager: CREATED");
        InitGame();
    }

    private void InitGame()
    {
        DontDestroyOnLoad(gameObject);
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = TargetFPS;
    }

    #endregion

    #region Properties

    public GState CurrentState
    {
        get
        {
            return _currentState; 
        }
    }

    #endregion

    #region Unity

    void Start()
    {
        #if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
        Debug.Log("Built-in VR Device present? " + UnityEngine.VR.VRDevice.isPresent);
        //Debug.Log("OpenVR Device present? " + OpenVR.IsHmdPresent());
        //Debug.Log("OpenVR Runtime installed? " + OpenVR.IsRuntimeInstalled());
        #endif

        #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
        //SteamVR.enabled = false;
        #endif

        ProcessEvent(GEvent.None);
    }

    void Update()
    {
        // stuff that can be done in all states
        if (Input.GetKeyDown(KeyCode.F12) || Input.GetKeyDown(KeyCode.Print))
        {
            Debug.Log("Screenshot_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".png saved!");
            Application.CaptureScreenshot("Screenshot_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".png");
        }

        // state dependent updates
        UpdateState(_currentState);
    }

    #endregion

    void OnDisable()
    {

    }

    public int GameScore(PlayerRole player)
    {
        return 0;
    }

    public void Clear()
    {
        #if LOG
        Debug.Log("GameManager: Clear (empty)");
        #endif
    }


    #region Statemachine

    // PRIVATE!!!
    private void SetState(GState state)
    {
        // run this only from within StateTransition()!
        ExitState(_currentState);
        _previousState = _currentState;
        _currentState = state;
        #if LOG
        Debug.Log("New State is [" + _currentState + "].");
        #endif
        EnterState(_currentState);
    }

    public void Continue(string s = "generic continue")
    {
        ProcessEvent(GEvent.Continue, s);
    }

    public void Cancel(string s = "generic cancel")
    {
        ProcessEvent(GEvent.Cancel, s);
    }

    public void Error(string s = "undefined error")
    {
        ProcessEvent(GEvent.Error, s);
    }

    // Controls, which transitions are allowed
    public void ProcessEvent(GEvent ev, string comment = "")
    {
        StopCoroutine(ProcessEvent());
        _ev = ev;
        _evComment = comment;
        StartCoroutine(ProcessEvent());
    }

    public IEnumerator ProcessEvent()
    {
        //yield return null;
        #if LOG
        Debug.Log("Event [" + _ev + "] in state [" + _currentState + "] -- " + _evComment + ".");
        #endif
        switch (_currentState)
        {
            case GState.Init:
                switch (_ev)
                {
                    case GEvent.None:
                        SetState(GState.Offline);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Running:
                switch (_ev)
                {
                    case GEvent.Pause:
                        SetState(GState.Paused);
                        break;
                    case GEvent.Reset:
                        SetState(GState.Reset);
                        break;
                    case GEvent.Win:
                    case GEvent.Loose:
                        SetState(GState.Intermission);
                        break;
                    case GEvent.Complete:
                        SetState(GState.Complete);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    case GEvent.Disconnect:
                        SetState(GState.Offline);
                        break;
                    case GEvent.Continue:
                        SetState(GState.Ready);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Paused: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Play:
                        SetState(GState.Running);
                        break;
                    case GEvent.Reset:
                        SetState(GState.Reset);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Complete: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Reset:
                        SetState(GState.Reset);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Intermission: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Play:
                    case GEvent.Continue:
                        SetState(GState.Running);
                        break;
                    case GEvent.Reset:
                        SetState(GState.Reset);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Help: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Reset:
                        SetState(GState.Reset);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Error: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Reset:
                        SetState(GState.Reset);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Offline: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Connect:
                        SetState(GState.Online);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Online: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Continue:
                        SetState(GState.Reset);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Reset: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Disconnect:
                        SetState(GState.Offline);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    case GEvent.Continue:
                        SetState(GState.Ready);
                        break;
                    default:
                        break;
                }
                break;
            case GState.Ready: // CURRENT STATE
                switch (_ev)
                {
                    case GEvent.Continue:
                    case GEvent.Play:
                        SetState(GState.Intermission);
                        break;
                    case GEvent.Disconnect:
                        SetState(GState.Offline);
                        break;
                    case GEvent.Error:
                        SetState(GState.Error);
                        break;
                    default:
                        break;
                }
                break;
            default:
                Debug.LogError("Should never get here!");
                break;
        }
        yield return null;
    }

    #endregion

    #region EnterState

    void OnEnterPausedState()
    {
        // do nothing
        SlotManager.singleton.HideAll();
    }

    void OnEnterInitState()
    {
        VideoInputManager.singleton.HideGhost();
    }

    void OnEnterRunningState()
    {
        ItemManager.singleton.SrvSpawn(NetGameManager.singleton.GameRound);
        SlotManager.singleton.HideAll();
        SlotManager.singleton.ShowAllInteractable();
    }

    void OnEnterCompleteState()
    {
    }

    void OnEnterIntermissionState()
    {
        //ItemManager.singleton.SrvClear();
        SlotManager.singleton.HideAll();
    }

    void OnEnterErrorState()
    {
        _errorTextTarget.text = _evComment;
    }

    void OnEnterHelpState()
    {
    }

    void OnEnterOfflineState()
    {
        //CameraRig.transform.SetParent(null);
        LeanTween.move(CameraRig, OfflineCameraPositionFar.position, 1f).setEase(LeanTweenType.easeInOutSine);
        LeanTween.rotateLocal(CameraRig, OfflineCameraPositionFar.rotation.eulerAngles, 1f).setEase(LeanTweenType.easeInOutSine);
        LeanTween.move(CameraRig, OfflineCameraPosition.position, 30f).setEase(LeanTweenType.easeInOutSine).setDelay(1f);
        LeanTween.rotateLocal(CameraRig, OfflineCameraPosition.rotation.eulerAngles, 30f).setEase(LeanTweenType.easeInOutSine).setDelay(1f);
    }

    void OnEnterResetState()
    {
        #if LOG
        Debug.Log("Reset: Removing Spawned Objects");
        #endif
        if (NetworkManager.singleton.IsClientConnected())
        {
            SlotManager.singleton.SrvClear();
            ItemManager.singleton.SrvClear();
            PlayerManager.singleton.Clear();
            GameManager.singleton.Clear();
            NetGameManager.singleton.Clear();
        }

        #if LOG
        Debug.Log("Reset: Setup Playfield");
        #endif

        UnityEngine.Random.InitState(2016);
        SlotManager.singleton.SrvSpawn();
        SlotManager.singleton.HideAll();
        LeanTween.move(CameraRig, ReadyCameraPosition.position, 2f).setEase(LeanTweenType.easeInOutSine);
        LeanTween.rotateLocal(CameraRig, ReadyCameraPosition.rotation.eulerAngles, 2f).setEase(LeanTweenType.easeInOutSine);

        #if LOG
        Debug.Log("Reset: Done");
        #endif
        ProcessEvent(GEvent.Continue);
    }

    void OnEnterOnlineState()
    {
        LeanTween.delayedCall(0.1f, () =>
            {
                ProcessEvent(GEvent.Continue);
            });
    }

    void OnEnterReadyState()
    {
        if (PlayerManager.singleton != null && PlayerManager.singleton.GetLocalPlayer() != null)
        {
            PlayerManager.singleton.GetLocalPlayer().CancelReady();
        }
        #if LOG
        Debug.Log("Ready: Waiting...");
        #endif
    }

    // calls state enter-event handlers
    void EnterState(GState state)
    {
        switch (state)
        {
            case GState.Paused:
                OnEnterPausedState();
                break;
            case GState.Init:
                OnEnterInitState();
                break;
            case GState.Running:
                OnEnterRunningState();
                break;
            case GState.Complete:
                OnEnterCompleteState();
                break;
            case GState.Intermission:
                OnEnterIntermissionState();
                break;
            case GState.Error:
                OnEnterErrorState();
                break;
            case GState.Help:
                OnEnterHelpState();
                break;
            case GState.Offline:
                OnEnterOfflineState();
                break;
            case GState.Reset:
                OnEnterResetState();
                break;
            case GState.Ready:
                OnEnterReadyState();
                break;
            case GState.Online:
                OnEnterOnlineState();
                break;
            default:
                Debug.LogError("Unknown State");
                break;
        }
        if (OnEnterState != null)
            OnEnterState.Invoke(state);
    }

    #endregion

    #region ExitState

    void OnExitPausedState()
    {
        // do nothing
    }

    void OnExitInitState()
    {
        // do nothing
    }

    void OnExitRunningState()
    {
        // Do nothing
    }

    void OnExitCompleteState()
    {
        // do nothing
    }

    void OnExitIntermissionState()
    {
        VideoInputManager.singleton.HideGhost();
    }

    void OnExitErrorState()
    {
        // do nothing
    }

    void OnExitHelpState()
    {
        // do nothing
    }

    void OnExitOfflineState()
    {
        LeanTween.cancel(CameraRig);
    }

    void OnExitResetState()
    {
        // do nothing
    }

    void OnExitOnlineState()
    {
        // do nothing
    }

    void OnExitReadyState()
    {
        // do nothing
    }

    // calls state exit-event handlers
    void ExitState(GState state)
    {
        if (OnExitState != null)
            OnExitState.Invoke(state);
        
        switch (state)
        {
            case GState.Paused:
                OnExitPausedState();
                break;
            case GState.Init:
                OnExitInitState();
                break;
            case GState.Running:
                OnExitRunningState();
                break;
            case GState.Complete:
                OnExitCompleteState();
                break;
            case GState.Intermission:
                OnExitIntermissionState();
                break;
            case GState.Error:
                OnExitErrorState();
                break;
            case GState.Help:
                OnExitHelpState();
                break;
            case GState.Offline:
                OnExitOfflineState();
                break;
            case GState.Ready:
                OnExitReadyState();
                break;
            case GState.Reset:
                OnExitResetState();
                break;
            case GState.Online:
                OnExitOnlineState();
                break;
            default:
                Debug.LogError("Unknown State");
                break;
        }
    }

    #endregion

    #region UpdateState

    void OnUpdatePausedState()
    {
        // do nothing
    }

    void OnUpdateInitState()
    {
        // do nothing
    }

    void OnUpdateCompleteState()
    {
        // do nothing
    }

    void OnUpdateIntermissionState()
    {
        // do nothing
    }

    void OnUpdateErrorState()
    {
        // do nothing
    }

    void OnUpdateHelpState()
    {
        // do nothing
    }

    void OnUpdateOfflineState()
    {
        // do nothing
    }

    void OnUpdateResetState()
    {
        // do nothing
    }

    void OnUpdateReadyState()
    {

    }

    void OnUpdateOnlineState()
    {
		
    }

    void OnUpdateRunningState()
    {
        SlotManager.singleton.GetScore(PlayerRole.Umbra);
    }

    // calls state exit-event handlers
    void UpdateState(GState state)
    {
        switch (state)
        {
            case GState.Paused:
                OnUpdatePausedState();
                break;
            case GState.Init:
                OnUpdateInitState();
                break;
            case GState.Running:
                OnUpdateRunningState();
                break;
            case GState.Complete:
                OnUpdateCompleteState();
                break;
            case GState.Intermission:
                OnUpdateIntermissionState();
                break;
            case GState.Error:
                OnUpdateErrorState();
                break;
            case GState.Help:
                OnUpdateHelpState();
                break;
            case GState.Offline:
                OnUpdateOfflineState();
                break;
            case GState.Reset:
                OnUpdateResetState();
                break;
            case GState.Ready:
                OnUpdateResetState();
                break;
            case GState.Online:
                OnUpdateOnlineState();
                break;
            default:
                Debug.LogError("Unknown State");
                break;
        }
        if (OnUpdateState != null)
            OnUpdateState.Invoke(state);
    }

    #endregion
}
