﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.Assertions;

//using Diag = System.Diagnostics;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
using Valve.VR;
#endif

[RequireComponent (typeof(Collider))]
[RequireComponent (typeof(Rigidbody))]
public class Grab : CustomNetworkBehaviour
{

	public Transform _carryAnchorUmbra;
	public Transform _carryAnchorLux;
	public GameObject _dropTargetMarker;
	public float _carriedScale = 0.5f;
	private Transform _carryAnchor;
	private Vector3 _originalScale;
	private Item _carriedItem;
	private Slot _dropTargetSlot;
	private Slot _originalSlot;
	private Vector3 _carryAnchorOriginalPos;
	//private ReadyHandler _inputManager;

	void Start ()
	{
		//_carryAnchorOriginalPos = carryAnchor.position;
		GetComponent<Collider> ().isTrigger = true;
		GetComponent<Rigidbody> ().isKinematic = true;

		Assert.IsNotNull (_dropTargetMarker);
		Assert.IsNotNull (_carryAnchor);
		Assert.IsNotNull (_carryAnchorLux);
		Assert.IsNotNull (_carryAnchorUmbra);

		_dropTargetMarker.SetActive (false);
		#if LOG
		#endif
		Debug.Log ("Grab: CREATED");
	}


	void OnEnterStateHandler (GState t)
	{
		switch (t) {
		case GState.Running:
			#if LOG
			Debug.Log ("Grab: Cancel added to OnEndTurn on " + gameObject.name + " entering " + t);
			#endif
			NetGameManager.singleton.OnEndTurn.AddListener (Cancel);
			break;
		case GState.Offline:
			#if LOG
			Debug.Log ("Grab: Cancel added to OnEndTurn on " + gameObject.name + " entering " + t);
			#endif
			NetGameManager.singleton.OnEndTurn.RemoveListener (Cancel);
			break;
		default:
			return;
		}
	}

	void OnExitStateHandler (GState t)
	{
		switch (t) {
		case GState.Running:
			#if LOG
			Debug.Log ("Grab: Cancel removed from OnEndTurn on " + gameObject.name + " exiting " + t);
			#endif
			NetGameManager.singleton.OnEndTurn.RemoveListener (Cancel);
			break;
		default:
			return;
		}
	}


	void OnEnable ()
	{
		GameManager.singleton.OnEnterState.AddListener (OnEnterStateHandler);
		GameManager.singleton.OnExitState.AddListener (OnExitStateHandler);
	}

	void OnDisable ()
	{
		GameManager.singleton.OnEnterState.RemoveListener (OnEnterStateHandler);
		GameManager.singleton.OnExitState.RemoveListener (OnExitStateHandler);
	}

	public void Cancel (int t)
	{
		if (_carriedItem != null) {
			#if LOG
			Debug.Log ("Grab: Cancelling " + _carriedItem + ". Fallback to " + _originalSlot.name);
			#endif
			Assert.IsNotNull (_originalSlot);
			_carriedItem.transform.localScale = _originalScale;
			_originalSlot.CatchItemAsPlayerMove (_carriedItem);
			_carriedItem = null;
			_dropTargetSlot = null;
			ScoreManager.singleton.ResetPreview ();
			SlotManager.singleton.HideAll ();
			SlotManager.singleton.ShowAllInteractable ();
			InputManager.singleton.Feedback (FeedbackPattern.BeepBeep);
		}
	}

	void Update ()
	{
		switch (GameManager.singleton.CurrentState) {
		case GState.Running:
			break;
		default:
			return;
		}

		// flip the cameraAnchorPosition when the camera is below the playing field
		if (GameManager.singleton.CameraRig.transform.position.y < transform.position.y)
			_carryAnchor = _carryAnchorLux;
		else
			_carryAnchor = _carryAnchorUmbra;

		// keep carried item aligned
		if (_carriedItem != null) {
			Assert.IsNotNull (_dropTargetMarker);
			_carriedItem.transform.rotation = Quaternion.identity;
		}

		HandleDropItem ();
	}

	void OnTriggerStay (Collider hit)
	{
		switch (GameManager.singleton.CurrentState) {
		case GState.Running:
			break;
		default:
			return;
		}

		HandleGrabItem (hit);
	}

	void OnTriggerEnter (Collider hit)
	{
		switch (GameManager.singleton.CurrentState) {
		case GState.Running:
			break;
		default:
			return;
		}
		HandleDropTarget (hit);

		if (_carriedItem != null) {
			SlotManager.singleton.HideAll ();
			SlotManager.singleton.ShowValidTargets (_carriedItem, PlayerManager.singleton.LocalRole);
		} else {
			SlotManager.singleton.HideAll ();
			SlotManager.singleton.ShowAllInteractable ();
		}
	}

	void HandleDropItem ()
	{
		// drop item
		if (InputManager.singleton.IsTriggerUp
		    && _carriedItem != null
		    && _dropTargetSlot != null) {
			Assert.IsNotNull (_dropTargetMarker);
			Assert.IsNotNull (_carryAnchor);
			Assert.IsNotNull (_carryAnchorLux);
			Assert.IsNotNull (_carryAnchorUmbra);
			Assert.IsNotNull (_dropTargetSlot);

			// drop item to dropTarget
			#if LOG
			Debug.Log ("Grab: Dropping " + _carriedItem + " to " + _dropTargetSlot.name);
			#endif
			_carriedItem.transform.localScale = _originalScale;
			_dropTargetSlot.CatchItemAsPlayerMove (_carriedItem);
			_carriedItem = null;
			_dropTargetSlot = null;
			//SlotManager.singleton.ShowValidTargets (_carriedItem, PlayerManager.singleton.LocalRole);
			ScoreManager.singleton.ResetPreview ();
			InputManager.singleton.Feedback (FeedbackPattern.Tick);
		}

		if (_dropTargetSlot != null) {
			_dropTargetMarker.SetActive (true);
			_dropTargetMarker.transform.position = _dropTargetSlot.transform.position;
		} else {
			_dropTargetMarker.SetActive (false);
		}
	}

	void HandleGrabItem (Collider hit)
	{
		// Grab item
		if (InputManager.singleton.IsTriggerDown) {
			if (_carriedItem != null) {
				//Debug.Log ("Grab: Still carrying something.");
				return;
			}
			
			Assert.IsNotNull (_dropTargetMarker);
			Assert.IsNotNull (_carryAnchor);
			Assert.IsNotNull (_carryAnchorLux);
			Assert.IsNotNull (_carryAnchorUmbra);

			// check what kind of trigger we have hit
			Item item = hit.gameObject.GetComponent<Item> ();

			// we got an item and it is interactable for us

			if (item != null
			    && item.CanInteract (PlayerManager.singleton.LocalRole)
			    && !PlayerManager.singleton.GetLocalPlayer ().HasMoved ()) {
				_originalSlot = item.GetSlot ();
				#if LOG
				Debug.Log ("Grab: Grabbing item " + item + " from slot " + _originalSlot);
				#endif
				if (_originalSlot != null) {
					_originalSlot.ReleaseItem (item);
				}
				item.DisableTrigger ();
				item.SetSlot (null);
				item.transform.SetParent (_carryAnchor);
				_carriedItem = item;
				_originalScale = _carriedItem.transform.localScale;
				_carriedItem.transform.localScale = Vector3.one * _carriedScale;
				//SlotManager.singleton.HideAll ();
				//SlotManager.singleton.ShowValidTargets (item, PlayerManager.singleton.LocalRole);
				InputManager.singleton.Feedback ();
			} else {
				#if LOG
				if (item == null)
					return;
				if (PlayerManager.singleton.GetLocalPlayer ().HasMoved ())
					Debug.Log ("Grab: Player already has moved this turn. " + PlayerManager.singleton.GetLocalPlayer () + ":to:" + PlayerManager.singleton.GetLocalPlayer ().MoveToSlot + ":from:" + PlayerManager.singleton.GetLocalPlayer ().MoveOfItem + ")");
				if (item.CanInteract (PlayerManager.singleton.LocalRole))
					Debug.Log ("Grab: Player cannot interact with this item. (" + PlayerManager.singleton.LocalRole + ")");
				#endif
			}
		}
	}

	void HandleDropTarget (Collider hit)
	{

		// update drop target
		if (_carriedItem != null) {

			// check what kind of trigger we have hit
			Slot slot = hit.gameObject.GetComponent<Slot> ();
			Item item = hit.gameObject.GetComponent<Item> ();

			if (item != null) {
				// if trigger is item --> ignore
			} else if (slot != null && slot.CanCatch (_carriedItem.Typus)) {
				// if trigger is compatible slot --> update drop target
				_dropTargetSlot = slot;
			} else {
				// ignore trigger
			}

			ScoreManager.singleton.PreviewSlot (_dropTargetSlot, _carriedItem);
		}
	}
}
