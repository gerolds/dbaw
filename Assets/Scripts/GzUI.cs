﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class GzUI : MonoBehaviour
{
    public Text _MainTarget;
    public Text _ModTarget;
    public Color _Default;
    public Color _Positive;
    public Color _Negative;

    private void Update()
    {
        int previewDelta = ScoreManager.singleton.previewGz - ScoreManager.singleton.gz;
        if (previewDelta < 0) {
            // red
            _ModTarget.text = previewDelta.ToString();
            _ModTarget.color = _Negative;
        } else if (previewDelta > 0) {
            // green
            _ModTarget.text = "+" + previewDelta.ToString();
            _ModTarget.color = _Positive;
        } else {
            // white
            _ModTarget.text = "";
            _ModTarget.color = _Default;
        }

        _MainTarget.text = ScoreManager.singleton.gz.ToString();
    }
}
