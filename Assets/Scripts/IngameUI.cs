﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IngameUI : MonoBehaviour
{
    #region Singleton


    public static IngameUI singleton = null;

    private void Awake()
    {
        // Singleton
        if (singleton == null) {
            singleton = this;
        } else if (singleton != this) {
            Destroy(gameObject);    
        }
        Debug.Log("InGameUI: CREATED");
    }

    #endregion

    public TextHandler MainDialogue;
}
