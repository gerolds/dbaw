﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using UnityEngine.Assertions;
using System;

public enum FeedbackPattern
{
	Beep,
	BeepBeep,
	LongBeep,
	Tick
}

class InputManager : MonoBehaviour
{
	#region SINGLETON

	public static InputManager singleton = null;

	private void Awake ()
	{
		// Singleton
		if (singleton == null) {
			singleton = this;
		} else if (singleton != this) {
			Destroy (gameObject);    
		}

	}

	#endregion

	public bool _isTrigger;
	public bool _isTriggerDown;
	public bool _isTriggerUp;

	public bool IsTrigger {
		get {
			return _isTrigger;
		}
	}

	public bool IsTriggerDown {
		get {
			return _isTriggerDown;
		}
	}

	public bool IsTriggerUp {
		get {
			return _isTriggerUp;
		}
	}

	void Start ()
	{
		//SteamVR_Controller.Input (3).TriggerHapticPulse (3000);
	}

	public void Feedback (FeedbackPattern p)
	{
		switch (p) {
		case FeedbackPattern.Beep:
			Feedback (500);
			break;
		case FeedbackPattern.BeepBeep:
			LeanTween.delayedCall (0.0f, () => Feedback (250));
			LeanTween.delayedCall (0.5f, () => Feedback (250));
			break;
		case FeedbackPattern.LongBeep:
			LeanTween.delayedCall (0.5f, () => Feedback (1000));
			break;
		case FeedbackPattern.Tick:
			LeanTween.delayedCall (0.0f, () => Feedback (250));
			break;
		default:
			throw new ArgumentOutOfRangeException ();
		}
	}

	public void Feedback (ushort duration = 500)
	{
		if (SteamVR_Controller.Input (1) != null)
			SteamVR_Controller.Input (1).TriggerHapticPulse (duration);
		if (SteamVR_Controller.Input (2) != null)
			SteamVR_Controller.Input (2).TriggerHapticPulse (duration);
		if (SteamVR_Controller.Input (3) != null)
			SteamVR_Controller.Input (3).TriggerHapticPulse (duration);
	}

	void Update ()
	{

// store trigger status (just for indirection)
		_isTriggerUp = Input.GetMouseButtonUp (0);
		_isTrigger = Input.GetMouseButton (0);
		_isTriggerDown = Input.GetMouseButtonDown (0);

//#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
		_isTriggerUp = (
		    SteamVR_Controller.Input (1).GetHairTriggerUp ()
		    || SteamVR_Controller.Input (2).GetHairTriggerUp ()
		    || SteamVR_Controller.Input (3).GetHairTriggerUp ()
		    || Input.GetMouseButtonUp (0)
		);
		_isTrigger = (
		    SteamVR_Controller.Input (1).GetHairTrigger ()
		    || SteamVR_Controller.Input (2).GetHairTrigger ()
		    || SteamVR_Controller.Input (3).GetHairTrigger ()
		    || Input.GetMouseButton (0)
		);
		_isTriggerDown = (
		    SteamVR_Controller.Input (1).GetHairTriggerDown ()
		    || SteamVR_Controller.Input (2).GetHairTriggerDown ()
		    || SteamVR_Controller.Input (3).GetHairTriggerDown ()
		    || Input.GetMouseButtonDown (0)
		);
//#endif
	}
}