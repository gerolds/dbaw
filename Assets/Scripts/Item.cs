﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[System.Flags]
public enum ItemTypus
{
    Effect = 1 << 0,
    Building = 1 << 1,
    Event = 1 << 2
}

public enum ItemElementum
{
    Round0 = 0,
    Round1 = 1,
    Round2 = 2,
    Round3 = 3
}

[RequireComponent(typeof(TouchFeedback))]
public class Item : NetGameBehaviour
{
    /* ____________________________________________________________________________
       Item is a passive data-class, which means all fields will be set from the
       object that currently owns the instance and that there are no internal
       callbacks except for the standard ones. The only active components are
       concerned with the world-ui, animations and other child behaviours
    */

    [SyncVar]
    public PlayerRole _ownerRole = PlayerRole.None;

    private BoxCollider trigger;

    private GameObject _marker;
    private GameObject _highlight;
    private TouchFeedback _touchFeedback;
    private TouchFX _highlightRig;
    private ItemText _statusText;
    private Vector3 _originalScale;

    // these will be calculated externally in SlotManager and only locally for the local players perspective
    private int _balancedGZ = 0;
    private int _balancedBIP = 0;

    public ItemTypus Typus;
    public ItemElementum Elementum = (ItemElementum)0;
    public int UmbraBIP = 1;
    public int UmbraGZ = 1;
    public int LuxBIP = 1;
    public int LuxGZ = 1;
    public GameObject HighlightPrefab;
    public TouchFX HighlightRigPrefab;
    public GameObject MarkerPrefab;
    public GameObject TextPrefab;

    // Texture is split in the middle, left side is UMBRA, right side is for its LUX representation
    // and gets shifted based on the local player's role.
    public Texture CardTexture;

    public float MarkerScaleInPlay = 2.0f;
    [Range(0, 1f)]
    public float MarkerScalePower = 1f;

    #region Networking

    //[SyncVar]
    private NetworkInstanceId _slot = NetworkInstanceId.Invalid;

    //[SyncVar]
    //private NetworkInstanceId _owningPlayer = NetworkInstanceId.Invalid;


    public Slot GetSlot()
    {
        if (_slot != NetworkInstanceId.Invalid)
            return NetworkUtility.FindLocalComponent<Slot>(_slot);
        else
            return null;
    }

    public void SetSlot(Slot slot)
    {
        if (slot == null)
            _slot = NetworkInstanceId.Invalid;
        else
            _slot = slot.netId;
    }

    #endregion

    #region Display

    float GetItemInfluence()
    {
        return (float)(Mathf.Abs(LuxBIP + LuxGZ) + Mathf.Abs(UmbraBIP + UmbraGZ)) / (float)ItemManager.Norm;
    }

    void Reveal()
    {
        // hide the highlight
        _highlightRig.gameObject.SetActive(true);
    }

    void Conceal()
    {
        // hide the highlight
        _highlightRig.gameObject.SetActive(false);
    }

    void HandleVisibility(PlayerRole localRole)
    {
        Slot slot = GetSlot();
        if (slot != null)
        {
            if (slot.IsInPlay)
            {
                Reveal();
            }
            if (slot.SlotType == SlotType.Deck)
            {
                Conceal();
            }
            if (slot.ownerRole == localRole)
            {
                Reveal();
            }
        }
    }

    void HandleStatusText(PlayerRole localRole)
    {
        // set item status text
        int BIPDelta = _balancedBIP - GetBIP(localRole);
        int GZDelta = _balancedGZ - GetGZ(localRole);
        _statusText.SetText(string.Format("{0}", BIPDelta));
        if (BIPDelta != 0 || GZDelta != 0)
            _statusText.gameObject.SetActive(true);
        else
            _statusText.gameObject.SetActive(false);
    }

    #endregion

    #region Gameplay

    // called after the item is caught by a slot
    public void OnDrop(Slot s)
    {
        this.transform.rotation = Quaternion.identity;

        if (_marker == null)
            return;

        if (s.IsInPlay)
        {
            #if LOG
            Debug.Log("Dropped into play. Inf:" + this.GetItemInfluence() + "");
            #endif
            _marker.transform.localScale = Vector3.one * this.MarkerScaleInPlay;
            _marker.transform.localScale += (Vector3.up * (float)this.GetItemInfluence() * this.MarkerScalePower);
        }
        else
        {
            _marker.transform.localScale = Vector3.one;
        }
    }

    // called before the item is released by a slot
    public void OnRelease()
    {
        if (_marker != null)
            _marker.transform.localScale = Vector3.one;
    }

    // set this when adding an item to a deck or whenever ownership changes
    [ServerCallback]
    public void SrvSetRole(PlayerRole role)
    {
        _ownerRole = role;
    }

    // set this when the local player role changes
    public void SetHighlightView(PlayerRole role)
    {
        // shift texture based on local player role 
        Material _highlightMat = _highlight.GetComponent<Renderer>().material;
        #if LOG
        Debug.Log("Item: SetView(" + role + ")");
        Debug.Log("Item: " + _highlightMat);
        #endif
        if (role == PlayerRole.Lux)
            _highlightMat.SetTextureOffset("_MainTex", new Vector2(0.5f, 0));
        else
            _highlightMat.SetTextureOffset("_MainTex", Vector2.zero);
    }

    public bool CanInteract(PlayerRole role)
    {
        if (_ownerRole == PlayerRole.None)
            Debug.LogError("Item has no role");

        Slot slot = GetSlot();
        if (slot == null)
            return false;
        else
            return _ownerRole == role && slot.SlotType == SlotType.Deck;
    }

    #endregion

    #region Scoring

    public int GetBIP(PlayerRole role)
    {
        switch (role)
        {
            case PlayerRole.Umbra:
                return UmbraBIP;
            case PlayerRole.Lux:
                return LuxBIP;
            default:
                throw new System.ArgumentOutOfRangeException();
        }
    }

    public int GetGZ(PlayerRole role)
    {
        switch (role)
        {
            case PlayerRole.Umbra:
                return UmbraGZ;
            case PlayerRole.Lux:
                return LuxGZ;
            default:
                throw new System.ArgumentOutOfRangeException();
        }
    }

    public int GetModifiedBIP(PlayerRole role)
    {
        Slot slot = GetSlot();
        if (slot == null)
            return GetBIP(role);

        if (slot.RowSlot == NetworkInstanceId.Invalid || slot.ColumnSlot == NetworkInstanceId.Invalid)
            return GetBIP(role);

        Item rowItem = GetSlot().GetRowSlot().GetItem();
        Item colItem = GetSlot().GetColumnSlot().GetItem();
        int modSupply = GetBIP(role);
        if (rowItem != null)
            modSupply += rowItem.GetBIP(role);
        if (colItem != null)
            modSupply += colItem.GetBIP(role);
        return modSupply;
    }

    public int GetModifiedGZ(PlayerRole role)
    {
        Slot slot = GetSlot();
        if (slot == null)
            return GetGZ(role);

        if (slot.RowSlot == NetworkInstanceId.Invalid || slot.ColumnSlot == NetworkInstanceId.Invalid)
            return GetGZ(role);

        Item rowItem = GetSlot().GetRowSlot().GetItem();
        Item colItem = GetSlot().GetColumnSlot().GetItem();
        int modDemand = GetGZ(role);
        if (rowItem != null)
            modDemand += rowItem.GetGZ(role);
        if (colItem != null)
            modDemand += colItem.GetGZ(role);
        return modDemand;
    }

    public int GetBalancedGZ(PlayerRole role)
    {
        return _balancedGZ;
    }

    public int GetBalancedBIP(PlayerRole role)
    {
        return _balancedBIP;
    }

    // use this if the balance scores were calculated externally
    public void SetBalancedScores(int bip, int gz)
    {
        //Debug.Log("Item [NetId:" + netId + "]: New balanced scores bip:" + bip + " gz:" + gz + "");
        _balancedGZ = gz;
        _balancedBIP = bip;
    }

    #endregion

    #region Unity


    void Update()
    {
        PlayerRole localRole = PlayerManager.singleton.LocalRole;
        HandleVisibility(localRole);
        HandleStatusText(localRole);
    }

    void Start()
    {
        SetupGameObject();
    }

    void SetupGameObject()
    {
        gameObject.name = gameObject.name + " [NetId:" + netId + "]";

        trigger = (BoxCollider)gameObject.AddComponent<BoxCollider>();
        trigger.size = new Vector3(2f, 0.5f, 2f);
        trigger.center = new Vector3(0, 1f, 0);
        trigger.isTrigger = true;

        _touchFeedback = GetComponent<TouchFeedback>();

        _highlightRig = ((TouchFX)Instantiate(
            HighlightRigPrefab, 
            HighlightRigPrefab.transform.localPosition, 
            HighlightRigPrefab.transform.localRotation, 
            this.transform)
        );

        _marker = (GameObject)Instantiate(
            MarkerPrefab, 
            MarkerPrefab.transform.localPosition, 
            MarkerPrefab.transform.localRotation, 
            this.transform
        );
        LeanTween.delayedCall(Random.Range(0, 1f), () => _marker.SendMessage("Grow"));

        _highlight = (GameObject)Instantiate(
            original: HighlightPrefab, 
            position: HighlightPrefab.transform.position, 
            rotation: HighlightPrefab.transform.localRotation, 
            parent: this.transform
        );

        _statusText = ((GameObject)Instantiate(
            original: TextPrefab, 
            position: TextPrefab.transform.position, 
            rotation: TextPrefab.transform.localRotation)
        ).GetComponent<ItemText>();
        _statusText.transform.SetParent(_highlightRig.Anchor);
        _highlight.transform.SetParent(_highlightRig.Anchor);

        // assign and shift texture based on local player role 
        Material _highlightMat = _highlight.GetComponent<Renderer>().material;
        _highlightMat.SetTexture("_EmissionMap", CardTexture);
        if (PlayerManager.singleton.LocalRole == PlayerRole.Lux)
            _highlightMat.SetTextureOffset("_MainTex", new Vector2(0.5f, 0));
        else
            _highlightMat.SetTextureOffset("_MainTex", Vector2.zero);

        // no null check because <TouchFeedback> is required component
        GetComponent<TouchFeedback>().Refresh();
    }

    public void EnableTrigger()
    {
        var comp = GetComponent<BoxCollider>();
        if (comp != null)
        {
            comp.center = Vector3.zero;
            //comp.enabled = true;
        }
    }

    public void DisableTrigger()
    {
        var comp = GetComponent<BoxCollider>();
        if (comp != null)
        {
            comp.center = Vector3.one * 100000f;
            //comp.enabled = false;
        }
    }

    #endregion

}
