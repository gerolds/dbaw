using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class ItemManager : CustomNetworkBehaviour
{

    public Item[] _umbraItems;
    public Item[] _luxItems;
    private List<Item> _umbraItemPool = new List<Item>();
    private List<Item> _luxItemPool = new List<Item>();
    public int _itemsPerPlayer = 5;
    public static int _norm = 20;
    public TextAsset _luxItemsCsv;
    public TextAsset _umbraItemsCsv;

    private List<Dictionary<string, object>> _luxParsedCsv;
    private List<Dictionary<string, object>> _umbraParsedCsv;
    private Item[] _playerItems;

    static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
    static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
    static char[] TRIM_CHARS = { '\"' };

    public static List<Dictionary<string, object>> ParseCSV(TextAsset data)
    {
        var list = new List<Dictionary<string, object>>();

        var lines = Regex.Split(data.text, LINE_SPLIT_RE);

        if (lines.Length <= 1)
            return list;

        var header = Regex.Split(lines[0], SPLIT_RE);
        for (var i = 1; i < lines.Length; i++)
        {

            var values = Regex.Split(lines[i], SPLIT_RE);
            if (values.Length == 0 || values[0] == "")
                continue;

            var entry = new Dictionary<string, object>();
            for (var j = 0; j < header.Length && j < values.Length; j++)
            {
                string value = values[j];
                value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "");
                object finalvalue = value;
                int n;
                float f;
                if (int.TryParse(value, out n))
                {
                    finalvalue = n;
                }
                else if (float.TryParse(value, out f))
                {
                    finalvalue = f;
                }
                entry[header[j]] = finalvalue;
            }
            list.Add(entry);
        }
        return list;
    }

    public static int Norm
    {
        get
        {
            return _norm;
        }
    }


    #region SINGLETON

    public static ItemManager singleton = null;

    void Awake()
    {
        Debug.Log("ItemManager: CREATED");
        // Singleton
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);    
        }
    }

    #endregion

    void Start()
    {
        SetNetParent(GetNetParent());
        SelfCheck();

//        _luxParsedCsv = ParseCSV(_luxItemsCsv);
//        _umbraParsedCsv = ParseCSV(_umbraItemsCsv);
//        foreach(var row in _luxParsedCsv) {
//            Debug.Log((int)row["UmbraBIP"]);
//        }
    }

    void SelfCheck()
    {
        Debug.Log("Item Manager: Rebuilding Decks");
        if (SlotManager.singleton.DeckSize < _itemsPerPlayer)
        {
            Debug.LogError(string.Format("Too few Deck-Slots available ({0}). {1} required.", SlotManager.singleton.DeckSize, _itemsPerPlayer));
            return;
        }
    }

    [ServerCallback]
    private Item SrvSpawnItem(int x, ref List<Item> pool)
    {
        GameObject newObj;
        newObj = (GameObject)Instantiate(
            original: pool[x].gameObject, 
            position: Vector3.zero, 
            rotation: pool[x].transform.rotation
        );
        Item newItem = newObj.GetComponent<Item>();
        newItem.AssignNetParent(this.netId);
//        newItem.SetNetParent(this.netId);
//        if (isServer) {
//        } else {
//            newItem.AssignNetParent(this.netId);
//        }
        NetworkServer.Spawn(newObj);
        return newItem;
    }

    public void SrvClear()
    {
        Debug.Log("Item Manager: Destroying Items");
//        var childObjects = transform.Cast<Transform>().ToList();
//        if (Application.isEditor) {
//            foreach (Transform child in childObjects) {
//                DestroyImmediate(child.gameObject);
//            }
//        } else {
//            foreach (Transform child in childObjects) {
//                Destroy(child.gameObject);
//            }
//        }

        Item[] objs = FindObjectsOfType<Item>();
        for (int i = 0; i < objs.Count(); i++)
        {
            if (Application.isEditor)
                DestroyImmediate(objs[i].gameObject);
            else
                Destroy(objs[i].gameObject);
        }
    }

    [ServerCallback]
    public void SrvSpawn(int round = 0)
    {
        Debug.Log("Item Manager: Spawing Items ---------------------------");
        _umbraItemPool.Clear();
        _luxItemPool.Clear();
        _playerItems = new Item[_itemsPerPlayer * 2];

        // move items from array to list and skip null references.
        if (_umbraItems.Length == 0)
        {
            Debug.LogError("Item Manager: Item pool empty.");
            return;
        }
        else
        {
            foreach (var item in _umbraItems)
            {
                if (item != null && (int)item.Elementum == round)
                {
                    _umbraItemPool.Add(item);
                }
            }
        }

        if (_luxItems.Length == 0)
        {
            Debug.LogError("Item Manager: Item pool empty.");
            return;
        }
        else
        {
            foreach (var item in _luxItems)
            {
                if (item != null && (int)item.Elementum == round)
                {
                    _luxItemPool.Add(item);
                }
            }
        }

        Debug.Log(string.Format("Item Manager: We have {0} and {1} items.", _umbraItemPool.Count, _luxItemPool.Count));

        int x = 0;
        Item newItem;
        for (int i = 0; i < _itemsPerPlayer; i++)
        {
            // Player Umbra
            x = UnityEngine.Random.Range(0, _umbraItemPool.Count - 1);
            newItem = SrvSpawnItem(x, ref _umbraItemPool);
            _playerItems[i] = newItem;
            SlotManager.singleton.SrvAddToDeck(PlayerRole.Umbra, newItem);

            // Player Lux
            x = UnityEngine.Random.Range(0, _luxItemPool.Count - 1);
            newItem = SrvSpawnItem(x, ref _luxItemPool);
            _playerItems[i + _itemsPerPlayer] = newItem;
            SlotManager.singleton.SrvAddToDeck(PlayerRole.Lux, newItem);
        }
        Debug.Log("Item Manager: Spawing Items Complete ---------------------------");
    }

    public void SetView(PlayerRole localRole)
    {
        Debug.Log("ItemManager: SetView");
        Item[] objs = FindObjectsOfType<Item>();
        Debug.Log("ItemManager: " + objs.Count() + " items found.");
        for (int i = 0; i < objs.Count(); i++)
        {
            objs[i].SetHighlightView(localRole);
        }
    }
}
