﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemText : MonoBehaviour
{
	public Text _Target;

	public void SetText (string txt)
	{
		_Target.text = txt;
	}
}
