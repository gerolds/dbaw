﻿using UnityEngine;
using UnityEngine.Assertions;
using System;
using System.Collections;
using System.Collections.Generic;

public enum Language
{
    DE,
    EN
}

public struct Phrase
{
    Phrase(Language l, string p)
    {
        this.lang = l;
        this.phrase = p;
    }

    public Language lang;
    public string phrase;
}

public class Lang : MonoBehaviour
{
    #region Singleton

    public static Lang singleton = null;

    private void Awake()
    {
        // Singleton
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);    
        }

        #if LOG
        Debug.Log("Lang: CREATED");
        #endif
    }

    #endregion

    public Language _Language = Language.DE;

    public static string Tr(string fstr, params object[] args)
    {
        return string.Format(fstr, args);
    }
}

