﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.Assertions;

public interface IGameBehaviour
{
    void EnterStateHandler(GState state);

    void UpdateStateHandler(GState state);

    void ExitStateHandler(GState state);

    void StartTurnHandler(int turn);

    void EndTurnHandler(int turn);

    void StartRoundHandler(int round);

    void EndRoundHandler(int round);
}

[RequireComponent(typeof(NetworkInstanceId))]
public abstract class NetGameBehaviour : CustomNetworkBehaviour
{
    #region Events

    protected virtual void OnDisable()
    {
        if (isServer)
        {
            GameManager.singleton.OnEnterState.RemoveListener(SrvEnterStateHandler);
            GameManager.singleton.OnExitState.RemoveListener(SrvExitStateHandler);
            GameManager.singleton.OnUpdateState.RemoveListener(SrvUpdateStateHandler);
        }
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        if (isServer)
        {
            GameManager.singleton.OnEnterState.AddListener(SrvEnterStateHandler);
            GameManager.singleton.OnExitState.AddListener(SrvExitStateHandler);
            GameManager.singleton.OnUpdateState.AddListener(SrvUpdateStateHandler);
        }
    }

    protected virtual void SrvEnterStateHandler(GState state)
    {
        switch (state)
        {
            case GState.Reset:
                NetGameManager.singleton.OnStartTurn.AddListener(SrvStartTurnHandler);
                NetGameManager.singleton.OnStartRound.AddListener(SrvStartRoundHandler);
                NetGameManager.singleton.OnEndTurn.AddListener(SrvEndTurnHandler);
                NetGameManager.singleton.OnEndRound.AddListener(SrvEndRoundHandler);
                break;
            case GState.Error:
            case GState.Offline:
                NetGameManager.singleton.OnStartTurn.RemoveListener(SrvStartTurnHandler);
                NetGameManager.singleton.OnStartRound.RemoveListener(SrvStartRoundHandler);
                NetGameManager.singleton.OnEndTurn.RemoveListener(SrvEndTurnHandler);
                NetGameManager.singleton.OnEndRound.RemoveListener(SrvEndRoundHandler);
                break;
            default:
                return;
        }
    }

    protected virtual void SrvUpdateStateHandler(GState state)
    {
        // do nothing
    }

    protected virtual void SrvExitStateHandler(GState state)
    {
        // do nothing
    }

    protected virtual void SrvStartTurnHandler(int turn)
    {
        // do nothing
    }

    protected virtual void SrvEndTurnHandler(int turn)
    {
        // do nothing
    }

    protected virtual void SrvStartRoundHandler(int round)
    {
        // do nothing
    }

    protected virtual void SrvEndRoundHandler(int round)
    {
        // do nothing
    }



    #endregion
}

public abstract class LocalGameBehaviour : MonoBehaviour
{
    #region Events

    private void OnEnable()
    {
        #if LOG
        Debug.Log ("GAME BEHAVIOUR ENABLED " + name + " -------------------------------------------------------------------------------------------------");
        #endif
        GameManager.singleton.OnEnterState.AddListener(EnterStateHandler);
        GameManager.singleton.OnUpdateState.AddListener(UpdateStateHandler);
        GameManager.singleton.OnExitState.AddListener(ExitStateHandler);
        switch (GameManager.singleton.CurrentState)
        {
            case GState.Offline:
            case GState.Online:
            case GState.Error:
            case GState.Init:
                break;
            case GState.Running:
            case GState.Paused:
            case GState.Complete:
            case GState.Intermission:
            case GState.Help:
            case GState.Reset:
            case GState.Ready:
                NetGameManager.singleton.OnStartTurn.AddListener(StartTurnHandler);
                NetGameManager.singleton.OnStartRound.AddListener(StartRoundHandler);
                NetGameManager.singleton.OnEndTurn.AddListener(EndTurnHandler);
                NetGameManager.singleton.OnEndRound.AddListener(EndRoundHandler);
                break;
            default:
                Debug.LogWarning("Unknown State");
                break;
        }
    }

    private void OnDisable()
    {
        #if LOG
        Debug.Log ("GAME BEHAVIOUR DISABLED " + name + " -------------------------------------------------------------------------------------------------");
        #endif

        GameManager.singleton.OnEnterState.RemoveListener(EnterStateHandler);
        GameManager.singleton.OnUpdateState.RemoveListener(UpdateStateHandler);
        GameManager.singleton.OnExitState.RemoveListener(ExitStateHandler);
        NetGameManager.singleton.OnStartTurn.RemoveListener(StartTurnHandler);
        NetGameManager.singleton.OnStartRound.RemoveListener(StartRoundHandler);
        NetGameManager.singleton.OnEndTurn.RemoveListener(EndTurnHandler);
        NetGameManager.singleton.OnEndRound.RemoveListener(EndRoundHandler);
    }

    protected virtual void EnterStateHandler(GState state)
    {
        #if LOG
        Debug.Log ("GAME BEHAVIOUR ENTER STATE " + name + " -------------------------------------------------------------------------------------------------");
        #endif

        switch (state)
        {
            case GState.Init:
            case GState.Error:
            case GState.Offline:
                NetGameManager.singleton.OnStartTurn.RemoveListener(StartTurnHandler);
                NetGameManager.singleton.OnStartRound.RemoveListener(StartRoundHandler);
                NetGameManager.singleton.OnEndTurn.RemoveListener(EndTurnHandler);
                NetGameManager.singleton.OnEndRound.RemoveListener(EndRoundHandler);
                break;
            default:
                return;
        }
    }

    protected virtual void UpdateStateHandler(GState state)
    {
        switch (state)
        {
            default:
                return;
        }
    }

    protected virtual void ExitStateHandler(GState state)
    {
        switch (state)
        {
            default:
                return;
        }
    }

    protected virtual void StartTurnHandler(int arg0)
    {
        // do nothing
    }

    protected virtual void EndTurnHandler(int arg0)
    {
        // do nothing
    }

    protected virtual void StartRoundHandler(int arg0)
    {
        // do nothing
    }

    protected virtual void EndRoundHandler(int arg0)
    {
        // do nothing
    }

    #endregion
}

