﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class LocalPlayerOnly : NetworkBehaviour
{
    void Awake()
    {
        if (isLocalPlayer) {
            gameObject.SetActive(false);
        }
    }
}
