﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class LowFPS : ImageEffectBase {

	private RenderTexture m_SavedTexture;
	public int TargetFPS = 8;


	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{

		if (m_SavedTexture == null)
			m_SavedTexture = Instantiate(source) as RenderTexture;

		if (Time.frameCount % TargetFPS == 0)
			Graphics.Blit(source, m_SavedTexture);

		Graphics.Blit(m_SavedTexture, destination);
	}
}
