﻿using UnityEngine;
using System.Collections;

public class MoveToRaycastCollision : MonoBehaviour
{

    public Camera Cam;

	void Update ()
	{
	    transform.position = Cam.ScreenToWorldPoint(new Vector3(0.5f, 0.5f, 0));
	}
}
