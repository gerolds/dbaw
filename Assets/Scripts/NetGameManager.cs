﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Assertions;
using System;

public sealed class NetGameManager : CustomNetworkBehaviour
{

    [SyncVar]
    private int _gameTime;
    [SyncVar]
    private int _remainingTurns;
    [SyncVar]
    private int _gameRound;

    public int GameTime
    {
        get
        {
            return _gameTime;
        }
    }

    public int GameTurn
    {
        get
        {
            return _remainingTurns;
        }
    }

    public int GameRound
    {
        get
        {
            return _gameRound;
        }
    }

    public int Rounds = 3;
    public int TurnsPerRound = 5;
    public int TurnTimeout = 20;
    private int lastFrameGameTime;

    public UnityEventInt OnStartTurn = new UnityEventInt();
    public UnityEventInt OnStartRound = new UnityEventInt();
    public UnityEventInt OnEndTurn = new UnityEventInt();
    public UnityEventInt OnEndRound = new UnityEventInt();

    #region Singleton

    public static NetGameManager singleton = null;

    private void Awake()
    {
        // Singleton
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);    
        }

        #if LOG
        Debug.Log("NetworkGameManager: CREATED");
        #endif
    }

    #endregion

    #region Turn Handlers

    [ServerCallback]
    void SrvOnStartRoundHandler(int r)
    {
        #if LOG
        Debug.Log("NetworkGameManager: Start of ROUND " + r + " ===================================");
        #endif
        RpcOnStartRound(_gameRound);
    }

    [ServerCallback]
    void SrvOnEndRoundHandler(int r)
    {
        #if LOG
        Debug.Log("NetworkGameManager: End of ROUND " + r + " =====================================");
        #endif
        RpcOnEndRound(_gameRound);
    }

    [ServerCallback]
    void SrvOnEndTurnHandler(int t)
    {
        #if LOG
        Debug.Log("NetworkGameManager: End of TURN " + t + " --------------------------------------");
        #endif

        if (isServer)
        {
            // execute the moves logged in all active players
            Player[] players = FindObjectsOfType<Player>();
            foreach (var p in players)
            {
                if (p.MoveToSlot != NetworkInstanceId.Invalid)
                {
                    Debug.Log("Executing player [" + PlayerManager.singleton.GetRole(p) + "]'s stored move: [" + p.MoveOfItem + " to " + p.MoveToSlot + "].");
                    NetworkUtility.FindLocalComponent<Slot>(p.MoveToSlot).SrvExecuteMove(p.MoveOfItem);
                    p.SrvResetMove();
                }
            }
            RpcOnEndTurn(_remainingTurns);
        }
    }

    [ServerCallback]
    void SrvOnStartTurnHandler(int t)
    {
        #if LOG
        Debug.Log("NetworkGameManager: Start of TURN " + t + " ------------------------------------");
        #endif

        if (isServer)
        {
            // clear the moves logged in all active players
            // unneccessary to do on the local player but we do it anyway.
            Player[] players = FindObjectsOfType<Player>();
            foreach (var p in players)
            {
                Assert.IsNotNull(p, "Don't know if this is trouble or not");
                if (p == null)
                    continue;
                p.SrvResetMove();
            }
            RpcOnStartTurn(_remainingTurns);
        }
    }

    #endregion

    #region Access

    public PlayerRole GetTurnPlayer()
    {
        if (_remainingTurns % 2 == 0)
        {
            return PlayerRole.Umbra;
        }
        else
        {
            return PlayerRole.Lux;
        }
    }

    #endregion

    public override void OnStartServer()
    {
        if (isServer)
        {
            #if LOG
            Debug.Log("NetGameManager: ON START SERVER");
            #endif
            GameManager.singleton.OnExitState.AddListener(SrvStateExitHandler);
            GameManager.singleton.OnUpdateState.AddListener(SrvStateUpdateHandler);
        }
    }

    void OnDisable()
    {
        if (isServer)
        {
            GameManager.singleton.OnExitState.RemoveListener(SrvStateExitHandler);
            GameManager.singleton.OnUpdateState.RemoveListener(SrvStateUpdateHandler);
        }
    }

    [ServerCallback]
    void SrvStateUpdateHandler(GState state)
    {
        switch (state)
        {
            case GState.Running:
                SrvUpdateTime(state);
                break;
            default:
                break;
        }
    }

    [ServerCallback]
    void SrvStateExitHandler(GState state)
    {
        if (!isServer)
            return;

        if (state == GState.Reset)
            _remainingTurns = TurnsPerRound;
    }

    [ServerCallback]
    void SrvUpdateTime(GState state)
    {
        if (!isServer)
        {
            return;
        }

        _gameTime = TurnTimeout - 1 - ((int)(Time.time) % TurnTimeout);
        // time changed --> do these only on a tick, not each frame!
        if (lastFrameGameTime != _gameTime)
        {
            //Debug.Log("NetGameManager: Tick " + _gameTime + " " + lastFrameGameTime);
            if (_gameTime == 0)
            {
                SrvOnEndTurnHandler(_remainingTurns);
                // this complicated bit makes it possible to interrupt the turn/round counting for an intermission
                if (_remainingTurns == TurnsPerRound)
                {
                    #if LOG
                    Debug.Log("NetGameManager: New Round");
                    #endif
                    SrvOnStartRoundHandler(_gameRound);
                    SrvOnStartTurnHandler(_remainingTurns);
                    RpcOnStartTurn(_remainingTurns);
                    _remainingTurns--;
                    SrvOnStartTurnHandler(_remainingTurns);
                    lastFrameGameTime = _gameTime;
                    return;
                }
                if (_remainingTurns == 0)
                {
                    #if LOG
                    Debug.Log("NetGameManager: Last Turn " + _remainingTurns);
                    #endif
                    SrvOnEndTurnHandler(_remainingTurns);
                    SrvOnEndRoundHandler(_gameRound);
                    _gameRound++;
                    _remainingTurns = TurnsPerRound;
                    return;
                }

                // else
                #if LOG
                Debug.Log("NetGameManager: Regular Turn " + _remainingTurns);
                #endif
                SrvOnStartTurnHandler(_remainingTurns);
                _remainingTurns--;
                SrvOnStartTurnHandler(_remainingTurns);
            }
        }
        lastFrameGameTime = _gameTime;
    }


    [ClientRpc]
    public void RpcOnEndTurn(int turn)
    {
        #if LOG
        Debug.Log("NetworkGameManager: RPC end turn " + turn);
        #endif
        if (OnEndTurn != null)
        {
            Debug.Log("Invoking...");
            OnEndTurn.Invoke(turn);
        }
        else
        {
            Debug.LogError("Event not initialized.");
        }
    }

    [ClientRpc]
    public void RpcOnStartTurn(int turn)
    {
        #if LOG
        Debug.Log("NetworkGameManager: RPC start turn " + turn);
        #endif

        if (OnStartTurn != null)
        {
            OnStartTurn.Invoke(turn);
        }
        else
        {
            Debug.LogError("Event not initialized.");
        }

        SlotManager.singleton.HideAll();
        SlotManager.singleton.ShowAllInteractable();

    }

    [ClientRpc]
    public void RpcOnEndRound(int round)
    {
        #if LOG
        Debug.Log("NetworkGameManager: RPC end round");
        #endif
        if (OnEndRound != null)
        {
            OnEndRound.Invoke(round);
        }
        else
        {
            Debug.LogError("Event not initialized.");
        }

        SlotManager.singleton.HideAll();
        GameManager.singleton.ProcessEvent(GEvent.Continue);
    }

    [ClientRpc]
    public void RpcOnStartRound(int round)
    {
        #if LOG
        Debug.Log("NetworkGameManager: RPC start round");
        #endif
        if (OnStartRound != null)
        {
            OnStartRound.Invoke(round);
        }
        else
        {
            Debug.LogError("Event not initialized.");
        }
    }

    public void Clear()
    {
        // nothing yet;
    }
}
