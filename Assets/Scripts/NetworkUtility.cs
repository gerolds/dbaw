﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public abstract class NetworkUtility
{
	public static T FindLocalComponent<T> (NetworkInstanceId ni) where T : Component
	{
		if (ni == NetworkInstanceId.Invalid)
			return null;
        
		T component = FindServerComponent<T> (ni);
		if (component == null)
			component = FindClientComponent<T> (ni);
		if (component == null) {
			Debug.Log ("f" + Time.frameCount + " Local component <" + typeof(T).ToString () + "> not found for id [" + ni + "]");
		}
		return component;
	}

	public static string NetIdStr (NetworkBehaviour b)
	{
		return "[NetId: " + b.netId + "]";
	}

	public static T FindServerComponent<T> (NetworkInstanceId ni) where T : Component
	{
		if (NetworkInstanceId.Invalid == ni)
			return null;

		GameObject obj = NetworkServer.FindLocalObject (ni);

		if (obj == null) {
			return null;
		}

		return obj.GetComponent<T> ();
	}

	public static T FindClientComponent<T> (NetworkInstanceId ni) where T : Component
	{
		if (NetworkInstanceId.Invalid == ni)
			return null;

		GameObject obj = ClientScene.FindLocalObject (ni);

		if (obj == null) {
			return null;
		}

		return obj.GetComponent<T> ();
	}

	public static bool SetParentFor (NetworkInstanceId child, NetworkInstanceId parent)
	{
		Transform p = FindLocalComponent<Transform> (parent);
		Transform c = FindLocalComponent<Transform> (child);
		if (p != null && c != null) {
			c.SetParent (p);
			return true;
		} else {
			Debug.Log ("Not found: child [" + child + "], parent [" + parent + "]");
			return false;
		}
	}
}

public abstract class CustomNetworkBehaviour : NetworkBehaviour
{
	[SyncVar]
	private NetworkInstanceId _netParent = NetworkInstanceId.Invalid;

	public override void OnStartClient ()
	{
		//Debug.Log("On Start Client " + gameObject.name);
		//if (!Application.isEditor)
		gameObject.name = gameObject.name + " [NetId: " + netId + "]";
        
		SetNetParent (_netParent);
	}

	public override void OnStartServer ()
	{
		//Debug.Log("On Start Server " + gameObject.name);
		//if (!Application.isEditor)
		//gameObject.name = gameObject.name + " [NetId: " + netId + "]";
		SetNetParent (_netParent);
	}

	public void AssignNetParent (NetworkInstanceId ni)
	{
		//Debug.Log("parent [" + ni + "] assigned to " + gameObject.name + " object [" + netId + "]");
		_netParent = ni;
	}

	[ClientRpc]
	protected void RpcSetNetParent (NetworkInstanceId ni)
	{
		_netParent = ni;
		if (_netParent != NetworkInstanceId.Invalid) {
			NetworkUtility.SetParentFor (this.netId, _netParent);
			return;
		}
		return;
	}

	public bool SetNetParent (NetworkInstanceId ni)
	{
		_netParent = ni;
		if (_netParent != NetworkInstanceId.Invalid) {
			//Debug.Log(gameObject.name + "[" + netId + "] SetNetParent(" + ni + ")");
			var b = NetworkUtility.SetParentFor (this.netId, _netParent);
			//Debug.Log("Set NetParent " + _netParent + " --> " + b);
			return b;
		}
		Debug.LogWarning ("Set NetParent " + ni + " --> invalid.");
		return false;
	}

	public NetworkInstanceId GetNetParent ()
	{
		NetworkBehaviour n = null;

		if (transform.parent != null)
			n = (NetworkBehaviour)transform.parent.GetComponent<NetworkBehaviour> ();

		if (n != null)
			return n.netId;
		else
			return NetworkInstanceId.Invalid;
	}
}


