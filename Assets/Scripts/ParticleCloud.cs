﻿using UnityEngine;
using System.Collections;

public class ParticleCloud : MonoBehaviour
{

    public WebCamTexture _Data;
    public ParticleSystem _ParticleSystem;

    private ParticleSystem.Particle[] _Particles;
    int width;
    int height;
    int pCount;
    int x, y;
    public float scale = 0.1f;
    public float depth = 1.0f;
    public int subsampling = 4;

    void Init()
    {
        if (_Data == null || _ParticleSystem == null && _Particles.Length != 0)
            return;
		
        width = _Data.width;
        height = _Data.height;
        _ParticleSystem.maxParticles = width * height / subsampling;
        _ParticleSystem.Emit(width * height / subsampling);
        _Particles = new ParticleSystem.Particle[width * height / subsampling];
        //Debug.Log ("Particles: " + (width / subsampling) + " " + _Particles.Length);
    }

    void LateUpdate()
    {
        Init();
        if (_Data != null)
        {
            pCount = _ParticleSystem.GetParticles(_Particles);
            for (int i = 0; i < _Particles.Length; i += subsampling)
            {
                x = i % width;
                y = i / width * subsampling;
                if (_Data.GetPixel(x, y).grayscale > 0.5f)
                    _Particles[i].position = Vector3.zero;
                else
                    _Particles[i].position = new Vector3(x * scale - width * scale / 2f, y * scale - height * scale / 2f, _Data.GetPixel(x, y).grayscale * depth);
            }
            _ParticleSystem.SetParticles(_Particles, pCount);
            //Debug.Log (_Particles.Length);
        }
    }
}
