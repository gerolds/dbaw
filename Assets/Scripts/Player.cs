﻿#if DEVELOPMENT_BUILD
//#define LOG
#endif

using System;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    public GameObject LocalOnly;

    // the current move to be locked in at turn's end
    [SyncVar]
    private NetworkInstanceId _moveOfItem = NetworkInstanceId.Invalid;
    [SyncVar]
    private NetworkInstanceId _moveToSlot = NetworkInstanceId.Invalid;
    [SyncVar]
    private bool _isReady;

    public UnityEventInt OnRoleChanged;

    public bool IsReady
    {
        get
        {
            return _isReady;
        }
    }

    public void CancelReady()
    {
        _isReady = false;
    }

    public NetworkInstanceId MoveOfItem
    {
        get
        {
            return _moveOfItem;
        }
    }

    public NetworkInstanceId MoveToSlot
    {
        get
        {
            return _moveToSlot;
        }
    }

    [ServerCallback]
    public void SrvResetMove()
    {
        #if LOG
		Debug.Log ("Player[" + PlayerManager.singleton.GetRole (this) + "]: Resetting stored move (Srv).");
        #endif
        if (isServer)
        {
            _moveOfItem = NetworkInstanceId.Invalid;
            _moveToSlot = NetworkInstanceId.Invalid;
            Debug.Log("Player: Move cleared (Srv) .");
        }
    }

    #region Unity

    void Awake()
    {
        BigMessage.singleton.ShowMessage("Joined");
    }

    void OnDestroy()
    {
        Debug.Log("OnDestroy");
    }

    void Update()
    {
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            //Camera.main.transform.SetParent(this.transform);
            //Camera.main.transform.localPosition = Vector3.zero;
            //Camera.main.transform.rotation = transform.rotation;
            //            Canvas canvas = GetComponentInChildren<Canvas>();
            //            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            //            canvas.worldCamera = Camera.main;
            LocalOnly.SetActive(true);

            LeanTween.cancel(GameManager.singleton.CameraRig);
            LeanTween.move(GameManager.singleton.CameraRig, GameManager.singleton.ReadyCameraPosition.position, 1f).setEase(LeanTweenType.easeInOutSine);
            LeanTween.rotateLocal(GameManager.singleton.CameraRig, GameManager.singleton.ReadyCameraPosition.rotation.eulerAngles, 1f).setEase(LeanTweenType.easeInOutSine);
        }
        else
        {
            LocalOnly.SetActive(false);

        }
        gameObject.name = "Player [NetID: " + netId + "]";
    }

    public bool HasMoved()
    {
        if (_moveOfItem == NetworkInstanceId.Invalid && _moveToSlot == NetworkInstanceId.Invalid)
            return false;
        else
            return true;
    }

    public override void OnStartClient()
    {
        NetGameManager.singleton.OnStartTurn.AddListener(OnStartTurn);
        NetGameManager.singleton.OnEndTurn.AddListener(OnEndTurn);
        NetGameManager.singleton.OnStartRound.AddListener(OnStartRound);
        NetGameManager.singleton.OnEndRound.AddListener(OnEndRound);
    }

    void OnDisable()
    {
        NetGameManager.singleton.OnStartTurn.RemoveListener(OnStartTurn);
        NetGameManager.singleton.OnEndTurn.RemoveListener(OnEndTurn);
        NetGameManager.singleton.OnStartRound.RemoveListener(OnStartRound);
        NetGameManager.singleton.OnEndRound.RemoveListener(OnEndRound);
    }

    #endregion

    #region Turns

    void OnStartTurn(int turn)
    {
        // Nothing
    }

    void OnEndTurn(int turn)
    {
        // Nothing
    }

    void OnStartRound(int turn)
    {
        // Nothing
    }

    void OnEndRound(int turn)
    {
        // Nothing
    }


    #endregion

    #region Networking Relay

    public void ClaimUmbra()
    {
        Debug.Log("Client Button Umbra");
        CmdClaimUmbra();
    }

    public void ClaimLux()
    {
        Debug.Log("Client Button Lux");
        CmdClaimLux();
    }

    public void SetMove(NetworkInstanceId slot, NetworkInstanceId item)
    {
        if (isLocalPlayer)
        {
            Debug.Log("Player: SetMove(" + slot + ", " + item + ")");
            CmdSetMove(slot, item);
        }

    }

    #endregion

    #region Server


    [Command]
    public void CmdSetMove(NetworkInstanceId slotNetId, NetworkInstanceId itemNetId)
    {
        // clear highligh if set
        Slot oldSlot = NetworkUtility.FindLocalComponent<Slot>(_moveToSlot);
        if (oldSlot != null)
            oldSlot.SetHighlight(false);

        // clear move
        if (slotNetId == NetworkInstanceId.Invalid && itemNetId == NetworkInstanceId.Invalid)
        {
            _moveOfItem = NetworkInstanceId.Invalid;
            _moveToSlot = NetworkInstanceId.Invalid;
            SlotManager.singleton.ShowAllInteractable();
            Debug.Log("Player: Move cleared.");
            return;
        }

        // if target slot is in play, set highlight and
        // store move else reset move
        Slot newSlot = NetworkUtility.FindLocalComponent<Slot>(slotNetId);
        if (newSlot.IsInPlay)
        {
            _moveOfItem = itemNetId;
            _moveToSlot = slotNetId;
            newSlot.SetHighlight(true);
            SlotManager.singleton.HideAll();
            return;
        }
        else
        {
            _moveOfItem = NetworkInstanceId.Invalid;
            _moveToSlot = NetworkInstanceId.Invalid;
            SlotManager.singleton.ShowAllInteractable();
            return;
        }

        throw new UnityException("Player: Invalid Move");
    }

    public void SetReady(bool b)
    {
        if (isLocalPlayer)
            CmdSetReady(b);
    }

    [Command]
    public void CmdSetReady(bool b)
    {
        _isReady = b;
    }

    [Command]
    public void CmdClaimUmbra()
    {
        #if LOG
		Debug.Log ("Server Button Umbra");
        #endif
        PlayerManager.singleton.SrvClaimRole(PlayerRole.Umbra, netId);
    }

    [Command]
    public void CmdClaimLux()
    {
        #if LOG
		Debug.Log ("Server Button Lux");
        #endif
        PlayerManager.singleton.SrvClaimRole(PlayerRole.Lux, netId);
    }

    #endregion

    public void SetPointOfView(Transform t, NetworkInstanceId lux, NetworkInstanceId umbra)
    {
        //DebugConsole.Log(netId + ": Client:SetPOV l:" + lux + ", u:" + umbra);

        if (isLocalPlayer)
        {

            if (netId == lux)
            {
                LeanTween.rotateY(PlayerManager.singleton._umbraUI.gameObject, 180f, 2f).setEase(LeanTweenType.easeInOutQuad);
//                PlayerManager.singleton.UmbraUI.SetActive(false);
//                PlayerManager.singleton.LuxUI.SetActive(true);
            }
            else
            {
                LeanTween.rotateY(PlayerManager.singleton._umbraUI.gameObject, 0, 2f).setEase(LeanTweenType.easeInOutQuad);
//                PlayerManager.singleton.UmbraUI.SetActive(true);
//                PlayerManager.singleton.LuxUI.SetActive(false);
            }
            #if LOG
			Debug.Log ("[" + netId + "] Claiming camera");
            #endif

            //GameManager.singleton.CameraRig.transform.rotation = rot;
            //GameManager.singleton.CameraRig.transform.position = pos;
            LeanTween.cancel(GameManager.singleton.CameraRig);
            LeanTween.move(GameManager.singleton.CameraRig, t.position, 1f).setEase(LeanTweenType.easeInOutSine);
            LeanTween.rotateLocal(GameManager.singleton.CameraRig, t.rotation.eulerAngles, 1f).setEase(LeanTweenType.easeInOutSine);
            if (OnRoleChanged != null)
                OnRoleChanged.Invoke(0);

            SlotManager.singleton.HideAll();
            SlotManager.singleton.ShowAllInteractable();
        }
    }
}

