﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{
	void Update()
	{
		if (!isLocalPlayer)
		{
			return;
		}
//		Debug.Log(Input.GetAxis("Horizontal"));

		var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
		var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

		//SetDirtyBit()

		transform.Rotate(0, x, 0);
		transform.Translate(0, 0, z);
	}
}