﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerManager : CustomNetworkBehaviour
{

    #region SINGLETON

    public static PlayerManager singleton = null;

    private void Awake()
    {
        // Singleton
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);    
        }

    }

    #endregion

    private NetworkInstanceId _umbraPlayerNetId = NetworkInstanceId.Invalid;
    private NetworkInstanceId _luxPlayerNetId = NetworkInstanceId.Invalid;

    private Player _luxPlayer;
    private Player _umbraPlayer;

    public Transform _umbraPlayerAnchor;
    public Transform _luxPlayerAnchor;
    public GameObject _umbraUI;
    public GameObject _luxUI;
    public GameCursor Cursor;

    public bool LocalPlayerReady
    {
        get
        {
            Player p = GetLocalPlayer();
            if (p != null)
                return GetLocalPlayer().IsReady;
            else
                return false;
        }
    }

    public void SetLocalPlayerReady(bool b)
    {
        Player p = GetLocalPlayer();
        if (p != null)
            p.SetReady(b);
    }

    //    public Player LocalPlayer {
    //        get {
    //            if (_low != null && _low.isLocalPlayer)
    //                return _low;
    //            else if (_high != null && _high.isLocalPlayer)
    //                return _high;
    //        }
    //    }

    #region Unity

    void Update()
    {
        if (!isServer)
            return;

        switch (GameManager.singleton.CurrentState)
        {
            case (GState.Ready):
                SrvContinueWhenReady();
                break;
            default:
                break;
        }
    }

    public override void OnStartServer()
    {
        if (!isServer)
            return;
        
        Debug.Log("PlayerManager: ON START SERVER");
    }

    //    public override void OnStartClient()
    //    {
    //        Debug.Log("PlayerManager:OnStartClient()");
    //    }

    void OnDisable()
    {
    }

    #endregion

    #region Access

    public Player GetUmbraPlayer()
    {
        return _umbraPlayer;
    }

    public Player GetLuxPlayer()
    {
        return _luxPlayer;
    }

    public Player GetLocalPlayer()
    {
        if (_luxPlayer != null && _luxPlayer.isLocalPlayer)
            return _luxPlayer;
        if (_umbraPlayer != null && _umbraPlayer.isLocalPlayer)
            return _umbraPlayer;
        return null;
    }

    public Player GetOtherPlayer(Player p)
    {
        if (_luxPlayer != null && _luxPlayer.Equals(p))
            return _umbraPlayer;
        if (_umbraPlayer != null && _umbraPlayer.Equals(p))
            return _luxPlayer;
        return null;
    }

    public PlayerRole LocalRole
    {
        get
        {
            if (_luxPlayer != null && _luxPlayer.isLocalPlayer)
                return PlayerRole.Lux;
            else if (_umbraPlayer != null && _umbraPlayer.isLocalPlayer)
                return PlayerRole.Umbra;
            //Debug.LogError ("Local player has no role");
            return PlayerRole.None;
        }
    }

    public PlayerRole GetRole(Player p)
    {
        if (_luxPlayer != null && _luxPlayer.Equals(p))
            return PlayerRole.Lux;
        if (_umbraPlayer != null && _umbraPlayer.Equals(p))
            return PlayerRole.Umbra;
        Debug.LogError("Player has no role");
        return PlayerRole.None;
    }

    #endregion

    public void UIRelayLocalClaimLux()
    {
        Player[] objs = FindObjectsOfType<Player>();
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].isLocalPlayer)
                objs[i].ClaimLux();
        }
    }

    public void UIRelayLocalClaimUmbra()
    {
        Player[] objs = FindObjectsOfType<Player>();
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].isLocalPlayer)
                objs[i].ClaimUmbra();
        }
    }

    #region Housekeeping

    public void Clear()
    {
        Debug.Log("Reset PlayerManager");
        _luxPlayer = null;
        _umbraPlayer = null;
        _luxPlayerNetId = NetworkInstanceId.Invalid;
        _umbraPlayerNetId = NetworkInstanceId.Invalid;
    }

    #endregion

    #region Client

    [ClientRpc]
    public void RpcPropagateRoles(NetworkInstanceId luxPlayer, NetworkInstanceId umbraPlayer)
    {
        Debug.Log("New lux player: [" + _luxPlayerNetId + "]");
        Debug.Log("New umbra player: [" + _umbraPlayerNetId + "]");
        _luxPlayerNetId = luxPlayer;
        _umbraPlayerNetId = umbraPlayer;

        _luxPlayer = NetworkUtility.FindLocalComponent<Player>(_luxPlayerNetId);
        _umbraPlayer = NetworkUtility.FindLocalComponent<Player>(_umbraPlayerNetId);



        if (_luxPlayer != null)
        {
            _luxPlayer.SetPointOfView(_luxPlayerAnchor, _luxPlayerNetId, _umbraPlayerNetId);
        }
        if (_umbraPlayer != null)
        {
            _umbraPlayer.SetPointOfView(_umbraPlayerAnchor, _luxPlayerNetId, _umbraPlayerNetId);
        }
        Debug.Log("New local role: [" + LocalRole + "]");

        ItemManager.singleton.SetView(LocalRole);
    }

    #endregion

    #region Server

    [ServerCallback]
    public void SrvClaimRole(PlayerRole role, NetworkInstanceId claimingPlayer)
    {
        Debug.Log("[" + claimingPlayer + "] is claiming role [" + role + "]");
        switch (role)
        {
            case PlayerRole.Umbra:
                if (_umbraPlayerNetId != claimingPlayer)
                {
                    if (_umbraPlayerNetId == NetworkInstanceId.Invalid)
                    {
                        _umbraPlayerNetId = claimingPlayer;
                        if (_luxPlayerNetId == _umbraPlayerNetId)
                            _luxPlayerNetId = NetworkInstanceId.Invalid;
                    }
                    else
                    {
                        Debug.Log("Switching roles");
                        _luxPlayerNetId = _umbraPlayerNetId;
                        _umbraPlayerNetId = claimingPlayer;                    
                    }
                    RpcPropagateRoles(_luxPlayerNetId, _umbraPlayerNetId);
                    SrvPropagateRoles(_luxPlayerNetId, _umbraPlayerNetId);
                }
                else
                {
                    Debug.Log("[" + claimingPlayer + "] already is [" + role + "]");
                }
                break;
            case PlayerRole.Lux:
                if (_luxPlayerNetId != claimingPlayer)
                {
                    if (_luxPlayerNetId == NetworkInstanceId.Invalid)
                    {
                        _luxPlayerNetId = claimingPlayer;
                        if (_luxPlayerNetId == _umbraPlayerNetId)
                            _umbraPlayerNetId = NetworkInstanceId.Invalid;
                    }
                    else
                    {
                        Debug.Log("Switching roles");
                        _umbraPlayerNetId = _luxPlayerNetId;
                        _luxPlayerNetId = claimingPlayer;
                    }
                    RpcPropagateRoles(_luxPlayerNetId, _umbraPlayerNetId);
                    SrvPropagateRoles(_luxPlayerNetId, _umbraPlayerNetId);
                }
                else
                {
                    Debug.Log("[" + claimingPlayer + "] already is [" + role + "]");
                }
                break;
            default:
                Debug.Log("Should not end up here!");
                break;
        }
    }

    [ServerCallback]
    public void SrvPropagateRoles(NetworkInstanceId low, NetworkInstanceId high)
    {
        Debug.Log("New lux player: [" + _luxPlayerNetId + "]");
        Debug.Log("New umbra player: [" + _umbraPlayerNetId + "]");
        _luxPlayerNetId = low;
        _umbraPlayerNetId = high;

        _luxPlayer = NetworkUtility.FindLocalComponent<Player>(_luxPlayerNetId);
        _umbraPlayer = NetworkUtility.FindLocalComponent<Player>(_umbraPlayerNetId);

        if (_luxPlayer != null)
            _luxPlayer.SetPointOfView(_luxPlayerAnchor, _luxPlayerNetId, _umbraPlayerNetId);
        if (_umbraPlayer != null)
            _umbraPlayer.SetPointOfView(_umbraPlayerAnchor, _luxPlayerNetId, _umbraPlayerNetId);
    }

    [ClientRpc]
    public void RpcRaiseEvent(int i, string c)
    {
        GameManager.singleton.ProcessEvent((GEvent)i, c + " rpc:s=" + isServer + ", c=" + isClient);
    }

    [ServerCallback]
    void SrvContinueWhenReady()
    {
        if (!isServer)
            return;
        
        // when both players signal they are ready (SyncVar), raise continue signal
        if (_umbraPlayer != null && _luxPlayer != null && _umbraPlayer.IsReady && _luxPlayer.IsReady)
        {
            Debug.Log("Both players ready");
            _umbraPlayer.CancelReady();
            _luxPlayer.CancelReady();
            RpcRaiseEvent((int)GEvent.Continue, "Both players ready");
        }
    }

    #endregion
}
