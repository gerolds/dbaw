﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerNameUI : MonoBehaviour
{

	public Text _TargetLocal;
	public Text _TargetRemote;

	void Update ()
	{
		Player localPlayer = PlayerManager.singleton.GetLocalPlayer ();
		if (localPlayer == null) {
			_TargetLocal.text = "LOCAL: Waiting...";
		} else {
			_TargetLocal.text = "LOCAL: server:" + localPlayer.isServer + ", ready:" + localPlayer.IsReady + ", role:" + PlayerManager.singleton.LocalRole + ".";
		}

		Player remotePlayer = PlayerManager.singleton.GetOtherPlayer (localPlayer);
		if (remotePlayer == null) {
			_TargetRemote.text = "REMOTE: Waiting...";
		} else {
			_TargetRemote.text = "REMOTE: server:" + remotePlayer.isServer + ", ready:" + remotePlayer.IsReady + ", role:" + PlayerManager.singleton.GetRole (remotePlayer) + ".";

		}
		
	}
}
