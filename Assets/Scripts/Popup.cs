﻿#if DEVELOPMENT_BUILD
//#define LOG
#endif

using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public class Popup : TouchFX
{
	private bool _isActive = false;
	public Transform _Anchor;

	private float _lerpStart;
	private Vector3 _startPos = Vector3.up * 0.5f;
	private Vector3 _startScale = Vector3.one * 0f;
	public Vector3 _idlePos = Vector3.up * 0.5f;
	public Vector3 _idleScale = Vector3.one * 0f;
	public Vector3 _endPos = Vector3.up * 3f;
	public Vector3 _endScale = Vector3.one * 1f;
	public float _duration = 0.25f;
	private float _progress = 0;
	private float _updown = 1f;
	//private float _parentScale;
	// to track which callback has been called this frame and used to prevent subsequent calls
	private int _flags = 0;

	void Awake ()
	{
		//transform.localScale = new Vector3(0, 0, 0);
		//_parentScale = transform.parent.localScale.magnitude;
	}

	void LateUpdate ()
	{
		_flags &= 0;

		_progress = Mathf.Clamp01 ((Time.time - _lerpStart) / _duration);

		if (GameManager.singleton.CameraRig.transform.position.y < transform.position.y) {
			_updown = -1f;
		} else {
			_updown = 1f;
		}

		// return to idle pos
		if (_isActive)
			return;
		transform.localScale = Vector3.Slerp (_startScale, _idleScale, _progress);
		transform.localPosition = Vector3.Slerp (_startPos, _idlePos * _updown * transform.parent.localScale.y * 2f, _progress);
		#if LOG
		Debug.Log (Time.frameCount + " Idle: " + transform.localScale + " " + _progress + " act:" + _isActive);
		#endif
	}

	public override void OnActiveEnter (object o)
	{
		#if LOG
		Debug.Log (Time.frameCount + " Enter: " + transform.localScale + " " + _progress + " act:" + _isActive);
		#endif
		if ((_flags & 1 << 0) != 0)
			return;
		_flags |= 1 << 0;
		_isActive = true;
		_lerpStart = Time.time - _duration * (1 - _progress);
		_progress = Mathf.Clamp01 ((Time.time - _lerpStart) / _duration);
		_startPos = transform.localPosition;
		_startScale = transform.localScale;
	}

	public override void OnActiveExit (object o)
	{
		#if LOG
		Debug.Log (Time.frameCount + " Exit: " + transform.localScale + " " + _progress + " act:" + _isActive);
		#endif
		if ((_flags & 1 << 1) != 0)
			return;
		_flags |= 1 << 1;
		_isActive = false;
		_lerpStart = Time.time - _duration * (1 - _progress);
		_startPos = transform.localPosition;
		_startScale = transform.localScale;
	}

	public override void OnActiveStay (object o)
	{
		#if LOG
		Debug.Log (Time.frameCount + " Stay: " + transform.localScale + " " + _progress + " act:" + _isActive);
		#endif
		if ((_flags & 1 << 2) != 0)
			return;
		_flags |= 1 << 2;
		_isActive = true;
		transform.localScale = Vector3.Slerp (_startScale, _endScale, _progress);
		transform.localPosition = Vector3.Slerp (_startPos, _endPos * _updown * transform.parent.localScale.y * 2f, _progress);
	}

	public override Transform Anchor {
		get {
			return _Anchor;
		}
	}
}
