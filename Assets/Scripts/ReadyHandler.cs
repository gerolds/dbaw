﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Assertions;

[RequireComponent (typeof(Player))]
public class ReadyHandler : NetworkBehaviour
{
	public KeyCode _ContinueKey;
	public KeyCode _CancelKey;
	private Player _player;

	public override void OnStartLocalPlayer ()
	{
		_player = GetComponent<Player> ();
	}

	void Start ()
	{
		if (!isLocalPlayer)
			this.enabled = false;
	}

	void Update ()
	{
		if (isLocalPlayer) {
			HandleInput ();
		}
	}

	void HandleInput ()
	{
		switch (GameManager.singleton.CurrentState) {
		case GState.Running:
			break;
		case GState.Ready:
			Assert.IsNotNull (_player);

			if (_player.IsReady) {
				IngameUI.singleton.MainDialogue.ShowMessage ("Waiting for other player...");
			} else {
				if (NetGameManager.singleton.GameRound == 0)
					IngameUI.singleton.MainDialogue.ShowMessage ("When Ready, press [" + _ContinueKey + "]");
				else
					IngameUI.singleton.MainDialogue.ShowMessage ("Round " + NetGameManager.singleton.GameRound + " complete\nWhen Ready, press [" + _ContinueKey + "]");
			}

			if (Input.GetKey (_ContinueKey)) {
				_player.CmdSetReady (true);
			}

			break;
		default:
			IngameUI.singleton.MainDialogue.ShowMessage ("");
			break;
		}
	}
}
