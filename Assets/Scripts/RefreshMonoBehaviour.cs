﻿using UnityEngine;
using System.Collections;

public abstract class RefreshMonoBehaviour : MonoBehaviour {
    protected virtual void OnEditorRefresh() {
		return;
	}
}