﻿using UnityEngine;
using UnityEngine.Networking;

public abstract class RefreshNetworkBehaviour : NetworkBehaviour {
	protected virtual void Refresh() {
		return;
	}
}
