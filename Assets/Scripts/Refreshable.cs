﻿using System;
using UnityEngine;

public class Refreshable : RefreshMonoBehaviour
{
    protected override void OnEditorRefresh()
    {
        Debug.Log("Refresh [" + gameObject.name + "]");
        SendMessage("Refresh");
    }
}

