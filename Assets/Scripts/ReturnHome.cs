﻿using UnityEngine;
using System.Collections;

public class ReturnHome : MonoBehaviour
{
	public Vector3 parentOffset;
	public float duration = 0.1f;

	void Update()
	{
		if (transform.parent == null)
			return;

		if (!LeanTween.isTweening(gameObject) && Vector3.Distance(transform.parent.position + parentOffset, transform.position) > 0.01f) {
			LeanTween.move(gameObject, transform.parent.position + parentOffset, duration);
		}
			
	}
}


