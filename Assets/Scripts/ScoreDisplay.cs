﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScoreDisplay : MonoBehaviour
{
	public Text targetText;
	//public Image targetImage;
	public PlayerRole player;

	void Update ()
	{
		int score = GameManager.singleton.GameScore (player);
		targetText.text = String.Format ("{0}", score);
	}
}
