﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

public class ScoreManager : CustomNetworkBehaviour
{
    #region SINGLETON

    public static ScoreManager singleton = null;

    void Awake()
    {
        // Singleton
        if (singleton == null) {
            singleton = this;
        } else if (singleton != this) {
            Destroy(gameObject);    
        }

        Debug.Log("ScoreManager: CREATED");
    }

    #endregion

    private int _balancedBIPTotal;
    private int _balancedGZTotal;
    private int _previewBIPTotal;
    private int _previewGZTotal;

    void OnDisable()
    {
        NetGameManager.singleton.OnEndTurn.RemoveListener(UpdateTotal);

    }

    public override void OnStartClient()
    {
        NetGameManager.singleton.OnEndTurn.AddListener(UpdateTotal);
    }

    public void UpdateTotal(int t)
    {
        SlotManager.singleton.UpdateScores(t);

        _balancedBIPTotal = 0;
        _balancedGZTotal = 0;
        Item[] allItems = FindObjectsOfType<Item>();
        for (int i = 0; i < allItems.Count(); i++) {
            _balancedBIPTotal += allItems[i].GetBalancedBIP(PlayerManager.singleton.LocalRole);
            _balancedGZTotal += allItems[i].GetBalancedGZ(PlayerManager.singleton.LocalRole);
        }
    }

    public void ResetPreview()
    {
        _previewBIPTotal = _balancedBIPTotal;
        _previewGZTotal = _balancedGZTotal;
    }

    public void PreviewSlot(Slot slot, Item item)
    {
        if (slot == null)
            return;
        
        if (!slot.IsInPlay)
            return;
        
        BipGz score = SlotManager.singleton.ScoreSlot(slot, item, PlayerManager.singleton.LocalRole);
        _previewBIPTotal = score.bip;
        _previewGZTotal = score.gz;
    }

    private void SetScore(int bip, int gz)
    {
        _balancedBIPTotal = bip;
        _balancedGZTotal = gz;
    }

    public int gz {
        get {
            return _balancedGZTotal;
        }
    }

    public int bip {
        get {
            return _balancedBIPTotal;
        }
    }

    public int previewGz {
        get {
            return _previewGZTotal;
        }
    }

    public int previewBip {
        get {
            return _previewBIPTotal;
        }
    }
}