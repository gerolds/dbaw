﻿#if DEVELOPMENT_BUILD
//#define LOG
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;

//#define LOG

[System.Flags]
public enum SlotType
{
	None = 1 << 0,
	Slot = 1 << 1,
	Row = 1 << 2,
	Column = 1 << 3,
	Deck = 1 << 4
}

[RequireComponent (typeof(Collider))]
public class Slot : CustomNetworkBehaviour
{

	[EnumFlag]
	public ItemTypus _acceptedItemTypes;

	#region Networking

	[SyncVar] public SlotType _slotType = SlotType.None;
	[SyncVar] private int _slotIdx = -1;
	//[SyncVar] public NetworkInstanceId _owningPlayer = NetworkInstanceId.Invalid;
	[SyncVar] public NetworkInstanceId _rowSlot = NetworkInstanceId.Invalid;
	[SyncVar] public NetworkInstanceId _columnSlot = NetworkInstanceId.Invalid;
	[SyncVar] private bool _isFounded = true;
	[SyncVar] public PlayerRole ownerRole;
	//[SyncVar] public bool _isInPlay = false;
    
	public GameObject _foundationPrefab;
	public GameObject _highlightPrefab;
	public Renderer _pinRenderer;
	public Renderer _handleRenderer;
	public Text _statusTarget;

	private NetworkInstanceId _itemInSlot = NetworkInstanceId.Invalid;
	private GameObject highlight;
	public GameObject foundation;

	public int gzRating;
	public int bipRating;

	public SlotType SlotType {
		get {
			return _slotType;
		}
	}

	//    public void AssignStructure(int index, SlotType type)
	//    {
	//        _slotIdx = index;
	//        _slotType = type;
	//    }

	public void AssignStructure (int slotIndex, SlotType slotType, PlayerRole ownerRole, NetworkInstanceId rowSlot, NetworkInstanceId columnSlot)
	{
		this._slotIdx = slotIndex;
		this._slotType = slotType;
		this.ownerRole = ownerRole;
		this._rowSlot = rowSlot;
		this._columnSlot = columnSlot;
	}

	public Item ItemInSlot {
		get {
			return NetworkUtility.FindLocalComponent<Item> (_itemInSlot);
		}
		set {
			if (value == null)
				_itemInSlot = NetworkInstanceId.Invalid;
			else
				_itemInSlot = value.netId;
		}
	}

	//    public Player Owner {
	//        get {
	//            return NetworkUtility.FindLocalComponent<Player>(_owningPlayer);
	//        }
	//        set {
	//            if (value == null)
	//                _owningPlayer = NetworkInstanceId.Invalid;
	//            else
	//                _owningPlayer = value.netId;
	//        }
	//    }


	public Slot GetRowSlot ()
	{
		return NetworkUtility.FindLocalComponent<Slot> (_rowSlot);
	}

	public NetworkInstanceId RowSlot {
		get {
			return _rowSlot;
		}
		set {
			_rowSlot = value;
		}
	}


	public Slot GetColumnSlot ()
	{
		return NetworkUtility.FindLocalComponent<Slot> (_columnSlot);
	}

	public NetworkInstanceId ColumnSlot {
		get {
			return _columnSlot;
		}
		set {
			_columnSlot = value;
		}
	}

	public void SetHighlight (bool b)
	{
		highlight.SetActive (b);
	}

	#endregion

	public void ShowPin ()
	{
		if (_pinRenderer != null)
			_pinRenderer.enabled = true;
	}

	public void HidePin ()
	{
		if (_pinRenderer != null)
			_pinRenderer.enabled = false;
	}

	public void ShowHandle ()
	{
		if (_handleRenderer != null)
			_handleRenderer.enabled = true;
	}

	public void HideHandle ()
	{
		if (_handleRenderer != null)
			_handleRenderer.enabled = false;
	}

	// wheter this slot accepts the given item type
	public bool IsCompatible (ItemTypus otherType)
	{
		return ((otherType & _acceptedItemTypes) != 0);
	}

	public bool IsInPlay {
		get {
			return (_slotType & (SlotType.Column | SlotType.Row | SlotType.Slot)) != 0;
		}
	}

	public bool IsFounded {
		get {
			return _isFounded;
		}
	}

	// wheter this slot is empty
	public bool IsEmpty {
		get { 
			#if LOG
			Debug.Log (NetworkUtility.NetIdStr (this) + " isEmpty(): " + (_itemInSlot == NetworkInstanceId.Invalid) + ", because [" + _itemInSlot + "]");
			#endif
			return (_itemInSlot == NetworkInstanceId.Invalid); 
		}
	}

	// returns the item currently occupying this slot
	public Item GetItem ()
	{
		return NetworkUtility.FindLocalComponent<Item> (_itemInSlot);
	}

	// wheter this slot accepts the given item type and is empty
	public bool CanCatch (ItemTypus otherType)
	{
		//Debug.Log(NetworkUtility.NetIdStr(this) + " isAccepting(" + otherType + "): isCompatible" + IsCompatible(otherType) + " && isEmpty:" + IsEmpty + " && isFounded:" + IsFounded);
		return (IsCompatible (otherType) && IsEmpty && IsFounded);
	}

	// releases an item (unregisters it occupying this slot)
	public void ReleaseItem (Item otherItem)
	{
		if (Equals (ItemInSlot.gameObject, otherItem.gameObject)) {
			ItemInSlot = null;
		}
	}

	public void ActivateFoundation ()
	{
		if (foundation != null) {
			foundation.SendMessage ("Grow", SendMessageOptions.RequireReceiver);
			_isFounded = true;
			#if LOG
			Debug.Log (name + " founded");
			#endif
		}
	}

	public void ResetFoundation ()
	{
		if (foundation != null) {
			foundation.SendMessage ("Shrink", SendMessageOptions.RequireReceiver);
			_isFounded = false;
			//Debug.Log(name + " de-founded");
		}
	}

	//    [ServerCallback]
	//    public void SrvClaimItem(NetworkInstanceId id) {
	//        RpcClaimItem(id);
	//    }

	// propagate a move to all clients, external server calls
	[ServerCallback]
	public void SrvExecuteMove (NetworkInstanceId id)
	{
		RpcExecuteMove (id);
	}

	// propagates a move to all clients
	[ClientRpc]
	private void RpcExecuteMove (NetworkInstanceId id)
	{
		CatchItem (id);
	}

	private void ActivateNeighbours ()
	{
		if (foundation != null) {
			Slot tmp;
			tmp = SlotManager.singleton.GetEastSlot (this);
			if (tmp != null) {
				tmp.ActivateFoundation ();
			}
			tmp = SlotManager.singleton.GetWestSlot (this);
			if (tmp != null) {
				tmp.ActivateFoundation ();
			}
			tmp = SlotManager.singleton.GetNorthSlot (this);
			if (tmp != null) {
				tmp.ActivateFoundation ();
			}
			tmp = SlotManager.singleton.GetSouthSlot (this);
			if (tmp != null) {
				tmp.ActivateFoundation ();
			}
		}
	}

	// locally moves items into slots
	public bool CatchItem (NetworkInstanceId id)
	{
		if (id == NetworkInstanceId.Invalid) {
			#if LOG
			Debug.Log ("[" + netId + "] CatchItem(" + id + "): is invalid");
			#endif
			//return false;
			return false;
		}

		if (id == _itemInSlot) {
			// seems like the item is already in this slot... for posterity, we'll assume it isn't.
		}

		Item item = NetworkUtility.FindLocalComponent<Item> (id);
		if (item == null) {
			#if LOG
			Debug.Log ("[" + netId + "] CatchItem(" + id + "): item not found");
			#endif
			return false;
		}

		if (CanCatch (item.Typus)) {
			Slot originalSlot = item.GetSlot ();
			item.OnRelease ();
			if (originalSlot != null) {
				originalSlot.ReleaseItem (item);
				#if LOG
				Debug.Log ("[" + netId + "] CatchItem(" + id + "): Original slot [" + originalSlot + "] released");
				#endif
			}

			ItemInSlot = item;
			item.EnableTrigger ();
			item.SetSlot (this);
			item.SetNetParent (this.netId);
			item.OnDrop (this);

			ActivateNeighbours ();

			return true;
		} else {
			#if LOG
			Debug.Log (NetworkUtility.NetIdStr (this) + " is not accepting [" + item.netId + "], because [" + _itemInSlot + "] is occupying it");
			#endif
			//return false;
			return false;
		}
	}

	// registers an item as occupying this slot
	public void CatchItemAsPlayerMove (Item otherItem)
	{
		if (CatchItem (otherItem.netId)) {
			// store this move for synchonisation in the local player object
			Player[] players = FindObjectsOfType<Player> ();
			foreach (var p in players) {
				if (p.isLocalPlayer) {
					#if LOG
					Debug.Log ("[" + netId + "] Move [" + otherItem + " to " + this.netId + "] stored.");
					#endif
					p.SetMove (slot: this.netId, item: otherItem.netId);
				}
			}
		}
		ScoreManager.singleton.UpdateTotal (0);
	}

	void Start ()
	{
		GetComponent<Collider> ().isTrigger = true;

		if (_foundationPrefab != null) {
			foundation = (GameObject)Instantiate (
				original: _foundationPrefab, 
				parent: this.transform,
				worldPositionStays: false);
			//_foundation.SendMessage("Grow");
		} else {
			//Debug.Log("Slot has no Foundation");
		}

		SlotManager.singleton.RegisterSlot (_slotIdx, _slotType, netId);

		highlight = (GameObject)Instantiate (
			_highlightPrefab, 
			this.transform);
		highlight.transform.localPosition = Vector3.zero;
		highlight.SetActive (false);

		HidePin ();
		HideHandle ();
	}

	void Update ()
	{
		if (_statusTarget != null) {
			if (bipRating != 0 || gzRating != 0)
				_statusTarget.text = string.Format ("{0}", bipRating);
			else
				_statusTarget.text = "";
		}
        
	}
}
