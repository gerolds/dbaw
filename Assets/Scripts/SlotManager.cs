﻿#if DEVELOPMENT_BUILD
//#define LOG
#endif

using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

public struct BipGz
{
	public int bip;
	public int gz;

	public BipGz (int bip, int gz)
	{
		this.bip = bip;
		this.gz = gz;
	}
}

public struct IntQuad
{
	int a;
	int b;
	int c;
	int d;

	public IntQuad (int a, int b, int c, int d)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
}

[ExecuteInEditMode]
public class SlotManager : CustomNetworkBehaviour
{
	public float _Spacing = 1.0f;
	public int _EventColumns = 6;
	public int _EffectRows = 3;
	public int _DeckRows = 2;
	public GameObject _SlotMarkerPrefab;
	public GameObject _RowMarkerPrefab;
	public GameObject _ColumnMarkerPrefab;
	public GameObject _DeckMarkerPrefab;
	public CityFoundation CityFoundation;

	private NetworkInstanceId[] _playerDecks;
	private NetworkInstanceId[] _eventColumnSlots;
	private NetworkInstanceId[] _effectRowSlots;
	private NetworkInstanceId[] _playerSlots;

	#region SINGLETON

	public static SlotManager singleton = null;

	void Awake ()
	{
		// Singleton
		if (singleton == null) {
			singleton = this;
		} else if (singleton != this) {
			Destroy (gameObject);    
		}
		#if LOG
		Debug.Log ("SlotManager: CREATED");
		#endif
		Init ();
	}

	void Init ()
	{
		InitializeFields ();
	}

	#endregion

	#region Unity

	public override void OnStartClient ()
	{
		//NetGameManager.singleton.OnEndTurn.AddListener(UpdateScores);
		base.OnStartClient ();
		#if LOG
		Debug.Log ("SlotManager: ON START CLIENT");
		#endif
	}

	public override void OnStartServer ()
	{
		#if LOG
		Debug.Log ("SlotManager: ON START SERVER");
		#endif
		SrvClear ();
	}

	public void OnDisable ()
	{
		//NetGameManager.singleton.OnEndTurn.RemoveListener(UpdateScores);
	}

	#endregion

	public int EffectRowCount {
		get {
			return _EffectRows;
		}
	}

	public float Spacing {
		get {
			return _Spacing;
		}
	}

	public int DeckSize {
		get {
			return _DeckRows * _EventColumns;
		}
	}

	public int EventColumnCount {
		get {
			return _EventColumns;
		}
	}

	//	public Slot GetRow (Slot slot)
	//	{
	//		int slotIdx = _playerSlots [(int)PlayerID.One].FindIndex (entry => entry.Equals (slot));
	//		if (slotIdx >= 0) {
	//			int rowIdx = slotIdx / (ColumnCount * 2);
	//		}
	//		return null;
	//	}

	//    public void Start()
	//    {
	//    }
	//
	//    public void ClaimSlots(Player player)
	//    {
	//
	//    }

	//[ServerCallback]
	public int GetScore (PlayerRole role)
	{
		if (_playerSlots == null)
			return -1;
		int score = 0;
		Slot slot;
		int nullCount = 0;
		foreach (NetworkInstanceId slotId in GetSlotsForRole(role)) {
			slot = NetworkUtility.FindLocalComponent<Slot> (slotId);
			if (slot == null) {
				nullCount++;
				continue;
			}
			Item item = slot.GetItem ();
			if (item != null)
				score += item.GetModifiedBIP (role);
		}
		#if LOG
		if (nullCount > 0)
			Debug.Log ("SlotManager: " + nullCount + " null-slots found while scoring");
		#endif
		return score;
	}

	public void SimulateItem (Slot slot, int bip, int gz)
	{
        
	}

	public BipGz ScoreSlot (Slot centerSlot, Item centerItem, PlayerRole role)
	{
		if (centerSlot == null || !centerSlot.IsInPlay)
			return(new BipGz (0, 0));

		Assert.IsNotNull (centerSlot);

		// calculate balance for this slot's item
		int centerBIPBalance = 0;
		int centerGZBalance = 0;
		Slot[] neigbours;
		int aNeighbourBIP = 0;
		int aNeighbourGZ = 0;
		Item aNeighbourItem;
		int i = 0;

		switch (centerSlot.SlotType) {
		case SlotType.None:
			// error
			Debug.LogError ("Slot has no type");
			break;
		case SlotType.Slot:
			neigbours = new Slot[4];
			neigbours [0] = GetNorthSlot (centerSlot);
			neigbours [1] = GetSouthSlot (centerSlot);
			neigbours [2] = GetEastSlot (centerSlot);
			neigbours [3] = GetWestSlot (centerSlot);
			foreach (Slot aNeighbour in neigbours) {
				aNeighbourBIP = 0;
				aNeighbourGZ = 0;

				aNeighbourItem = null;
				if (aNeighbour != null) {
					Assert.IsNotNull (aNeighbour);
					Assert.IsNull (aNeighbourItem);

					aNeighbourItem = aNeighbour.GetItem ();
					if (aNeighbourItem != null) {
						Assert.IsNotNull (aNeighbourItem);
						aNeighbourBIP = aNeighbourItem.GetModifiedBIP (role);
						aNeighbourGZ = aNeighbourItem.GetModifiedGZ (role);
					}
					centerBIPBalance += aNeighbourBIP;
					centerGZBalance += aNeighbourGZ;
				}
			}
			if (centerItem != null) {
				Assert.IsNotNull (centerItem);
				int centerBIPRaw = centerItem.GetBIP (role);
				int centerBIP = centerItem.GetModifiedBIP (role);
				int centerGZRaw = centerItem.GetGZ (role);
				int centerGZ = centerItem.GetModifiedGZ (role);
				centerBIPBalance = centerBIP;
				centerGZBalance = centerGZ;
				#if false
				if (centerBIP > 0)
				centerBIPBalance = Mathf.Clamp(centerBIPBalance, 0, centerBIP);
				if (centerBIP < 0)
				centerBIPBalance = Mathf.Clamp(centerBIPBalance, centerBIP, 0);
				if (centerBIP == 0)
				centerBIPBalance = 0;

				if (centerGZ > 0)
				centerGZBalance = Mathf.Clamp(centerGZBalance, 0, centerGZ);
				if (centerGZ < 0)
				centerGZBalance = Mathf.Clamp(centerGZBalance, centerGZ, 0);
				if (centerGZ == 0)
				centerGZBalance = 0;
				#endif
			}
			break;
		case SlotType.Row:
			// add center items value to all items in that row
			neigbours = new Slot[EventColumnCount];
			i = 0;
			foreach (NetworkInstanceId iterSlot in _playerSlots) {
				if (iterSlot == NetworkInstanceId.Invalid)
					continue;

				NetworkInstanceId iterRowSlot = NetworkUtility.FindLocalComponent<Slot> (iterSlot).RowSlot;

				if (iterSlot.Equals (centerSlot.netId))
					neigbours [i] = NetworkUtility.FindLocalComponent<Slot> (iterRowSlot);
				i++;
			}
			foreach (Slot aNeighbour in neigbours) {
				if (aNeighbour == null)
					continue;
				
				aNeighbourBIP = 0;
				aNeighbourGZ = 0;
				aNeighbourItem = null;

				Assert.IsNotNull (aNeighbour);
				Assert.IsNull (aNeighbourItem);

				aNeighbourItem = aNeighbour.GetItem ();
				if (aNeighbourItem != null) {
					Assert.IsNotNull (aNeighbourItem);
					aNeighbourBIP = aNeighbourItem.GetBIP (role);
					aNeighbourGZ = aNeighbourItem.GetGZ (role);
				}
				centerBIPBalance += aNeighbourBIP + centerItem.GetBIP (role);
				centerGZBalance += aNeighbourGZ + centerItem.GetGZ (role);
			}
			break;
		case SlotType.Column:
			// add center items value to all items in that column
			neigbours = new Slot[EffectRowCount];
			i = 0;
			foreach (NetworkInstanceId iterSlot in _playerSlots) {
				if (iterSlot == NetworkInstanceId.Invalid)
					continue;

				NetworkInstanceId iterColumnSlot = NetworkUtility.FindLocalComponent<Slot> (iterSlot).ColumnSlot;

				if (iterSlot.Equals (centerSlot.netId))
					neigbours [i] = NetworkUtility.FindLocalComponent<Slot> (iterColumnSlot);
				i++;
			}
			foreach (Slot aNeighbour in neigbours) {
				aNeighbourBIP = 0;
				aNeighbourGZ = 0;

				aNeighbourItem = null;
				if (aNeighbour != null) {
					Assert.IsNotNull (aNeighbour);
					Assert.IsNull (aNeighbourItem);

					aNeighbourItem = aNeighbour.GetItem ();
					if (aNeighbourItem != null) {
						Assert.IsNotNull (aNeighbourItem);
						aNeighbourBIP = aNeighbourItem.GetBIP (role);
						aNeighbourGZ = aNeighbourItem.GetGZ (role);
					}
					centerBIPBalance += aNeighbourBIP + centerItem.GetBIP (role);
					centerGZBalance += aNeighbourGZ + centerItem.GetGZ (role);
				}
			}
			break;
		case SlotType.Deck:
			// skip
			break;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}

		return(new BipGz (centerBIPBalance, centerGZBalance));
	}

	// this is part of the slot manager to enable a dynamic programming approach to resolving the queries (eventually)
	public void UpdateScores (int turn)
	{
		#if LOG
		Debug.Log ("SlotManager: Calculating Scores in turn " + turn + " for " + _playerSlots.Count () + " registered slots.");
		#endif
		Assert.IsNotNull (PlayerManager.singleton, "SlotManager: Are we offline?");
		PlayerRole role = PlayerManager.singleton.LocalRole;

		foreach (NetworkInstanceId id in _playerSlots) {

			// all registered ids should also be present... if not, this assert indicated dirty data
			Assert.IsFalse (id == NetworkInstanceId.Invalid, "SlotManager: Invalid netId encountered");
			if (id == NetworkInstanceId.Invalid)
				continue;

			// get the slot from the net id... should require no null test as the id should be derived
			// from server-spawned NetworkIdentities
			Slot centerSlot = NetworkUtility.FindLocalComponent<Slot> (id);
			Assert.IsNotNull (centerSlot, "SlotManager: Can't find Slot component for [NetId:" + id + "]");
			Item centerItem = centerSlot.GetItem ();

			// score the slot, and item
			BipGz centerBalance = ScoreSlot (centerSlot, centerItem, PlayerManager.singleton.LocalRole);
			centerSlot.bipRating = centerBalance.bip;
			centerSlot.gzRating = centerBalance.gz;
			if (centerItem != null) {
				// store the calculated values in the item
				centerItem.SetBalancedScores (centerBalance.bip, centerBalance.gz);
			}
		}
	}

	public void ShowValidTargets (Item item, PlayerRole role)
	{
		if (item == null) {
			Debug.LogWarning ("SlotManager: Method called with null argument. Ignoring.");
			return;
		}
		Slot[] objs = FindObjectsOfType<Slot> ();
		for (int i = 0; i < objs.Count (); i++) {
			if (objs [i].CanCatch (item.Typus)
			    && (objs [i].ownerRole == role || objs [i].ownerRole == PlayerRole.None)) {
				objs [i].ShowPin ();
			} else {
				objs [i].HidePin ();
			}
		}
	}

	public void HideAll ()
	{
		#if LOG
		Debug.Log ("SlotManager: Hiding all slot markers");
		#endif
		Slot[] objs = FindObjectsOfType<Slot> ();
		for (int i = 0; i < objs.Count (); i++) {
			objs [i].HidePin ();
			objs [i].HideHandle ();
		}
	}

	public void ShowAllInteractable ()
	{
		if (PlayerManager.singleton.GetLocalPlayer ().HasMoved ()) {
			#if LOG
			Debug.Log ("SlotManager: Player has moved, nothing is interactable.");
			#endif
			return;
		}

		#if LOG
		Debug.Log ("SlotManager: Unhinding interactable markers");
		#endif

		int iip = 0;
		int ie = 0;
		int ni = 0;
		int lr = 0;
		int st = 0;
		Slot[] slots = FindObjectsOfType<Slot> ();
		for (int i = 0; i < slots.Count (); i++) {
			if (slots [i].IsInPlay) {
				iip++;
				continue;
			}
			if (slots [i].IsEmpty) {
				ie++;
				continue;
			}
			if (slots [i].ownerRole == PlayerManager.singleton.LocalRole && slots [i].SlotType == SlotType.Deck) {
				slots [i].ShowHandle ();
				st++;
			} else {
				lr++;
			}
		}
		#if LOG
		Debug.Log ("SlotManager: Of " + slots.Count () + " slots " + iip + " are in play, " + ie + " are empty, " + lr + " are restricted by role, " + st + " are interactable");
		#endif
	}

	public void UnhideAll ()
	{
		#if LOG
		Debug.Log ("SlotManager: Unhiding all slot markers");
		#endif
		Slot[] objs = FindObjectsOfType<Slot> ();
		for (int i = 0; i < objs.Count (); i++) {
			objs [i].ShowPin ();
			objs [i].ShowHandle ();
		}
	}

	public void ResetAllFoundations ()
	{
		#if LOG
		Debug.Log ("SlotManager: Resetting slot foundations");
		#endif
		Slot[] objs = FindObjectsOfType<Slot> ();
		int j = 0;
		for (int i = 0; i < objs.Count (); i++) {
			LeanTween.delayedCall (
				this.gameObject, 
				Random.Range (0, 5f), 
				() => {
					objs [j].ResetFoundation ();
					j++;
				}
			);
		}
	}

	public void QuicklyResetAllFoundations ()
	{
		#if LOG
		Debug.Log ("SlotManager: Resetting slot foundations");
		#endif
		Slot[] objs = FindObjectsOfType<Slot> ();
		int j = 0;
		for (int i = 0; i < objs.Count (); i++) {
			LeanTween.delayedCall (
				this.gameObject, 
				Random.Range (0, 0.5f), 
				() => {
					objs [j].ResetFoundation ();
					j++;
				}
			);
		}
	}

	public void FoundCenterTile ()
	{
		NetworkUtility.FindLocalComponent<Slot> (_playerSlots [EffectRowCount * (EventColumnCount / 2)]).ActivateFoundation ();
		NetworkUtility.FindLocalComponent<Slot> (_playerSlots [EffectRowCount * (EventColumnCount / 2) + (EventColumnCount * EffectRowCount)]).ActivateFoundation ();
	}

	#region Slot Access

	bool SlotExists (int row, int column)
	{
		if (row >= EffectRowCount || row < 0)
			return false;
		if (column >= EventColumnCount || column < 0)
			return false;
		return true;
	}

	private static int GetIndex<T> (T[] haystack, T needle)
	{
		if (haystack == null)
			return -1;
		for (int i = 0; i < haystack.Length; i++) {
			if (haystack [i].Equals (needle))
				return i;
		}
		return -1;
	}

	public int GetRowIndex (Slot slot)
	{
		return GetIndex<NetworkInstanceId> (_effectRowSlots, slot.netId);
	}

	public int GetColumnIndex (Slot slot)
	{
		return GetIndex<NetworkInstanceId> (_eventColumnSlots, slot.netId);
	}

	Slot GetSlotAt (int row, int column)
	{
		if (SlotExists (row, column))
			return NetworkUtility.FindLocalComponent<Slot> (_playerSlots [(_EventColumns * _EffectRows) + row + EffectRowCount * column]);
		else
			return null;
	}

	Slot GetSlotAt (int offset)
	{
		if (offset < _playerSlots.Length)
			return NetworkUtility.FindLocalComponent<Slot> (_playerSlots [offset]);
		else
			return null;
	}

	public Slot GetEastSlot (Slot slot)
	{
		if (!slot.IsInPlay)
			return null;
        
		int idx = GetIndex<NetworkInstanceId> (_playerSlots, slot.netId);
		if (idx == -1)
			Debug.LogError ("SlotManager: Slot not Found.");

		int slotsPerSide = EffectRowCount * EventColumnCount;

		if (idx % slotsPerSide > slotsPerSide - EffectRowCount)
			return null;

		return GetSlotAt (idx + EffectRowCount);
	}

	public Slot GetWestSlot (Slot slot)
	{
		if (!slot.IsInPlay)
			return null;
        
		int idx = GetIndex<NetworkInstanceId> (_playerSlots, slot.netId);
		if (idx == -1)
			Debug.LogError ("SlotManager: Slot not Found.");
        
		int slotsPerSide = EffectRowCount * EventColumnCount;
		if (idx % slotsPerSide < EffectRowCount)
			return null;

		return GetSlotAt (idx - EffectRowCount);
	}

	// towards edge
	public Slot GetNorthSlot (Slot slot)
	{
		if (!slot.IsInPlay)
			return null;
        
		int idx = GetIndex<NetworkInstanceId> (_playerSlots, slot.netId);
		if (idx == -1)
			Debug.LogError ("SlotManager: Slot not Found.");
        
		//int slotsPerSide = RowCount * ColumnCount;
		//int totalSlotCount = slotsPerSide * 2;
		// outside edge
		if (idx % EffectRowCount == EffectRowCount - 1) {
			return null;
		}

		return GetSlotAt (idx + 1);
	}

	// towards middle
	public Slot GetSouthSlot (Slot slot)
	{
		if (!slot.IsInPlay)
			return null;
        
		int idx = GetIndex<NetworkInstanceId> (_playerSlots, slot.netId);
		if (idx == -1)
			Debug.LogError ("SlotManager: Slot not Found.");
        
		int slotsPerSide = EffectRowCount * EventColumnCount;
		int totalSlotCount = slotsPerSide * 2;

		// outside edge
		//        if (idx % RowCount == RowCount - 1) {
		//            idx = idx + (RowCount * ColumnCount) % (RowCount * ColumnCount * 2);
		//            return GetSlotAt(idx);
		//        }

		if (idx % EffectRowCount == 0) {
			idx = (idx + slotsPerSide) % totalSlotCount;
			return GetSlotAt (idx);
		}

		return GetSlotAt (idx - 1);
	}

	public NetworkInstanceId[] GetSlotsForRole (PlayerRole player)
	{
		return GetSubArr<NetworkInstanceId> (_playerSlots, (int)player, 2);
	}

	//    public Slot[] GetPlayerRows(PlayerRole player)
	//    {
	//        return GetSubArr<Slot>(_playerRows, (int)player, 2);
	//    }
	//
	//    public Slot[] GetPlayerColumns(PlayerRole player)
	//    {
	//        return GetSubArr<Slot>(_playerColumns, (int)player, 2);
	//    }
	//
	//    public Slot[] GetPlayerDeck(PlayerRole player)
	//    {
	//        return GetSubArr<Slot>(_playerDecks, (int)player, 2);
	//    }

	public static T[] GetSubArr<T> (T[] arr, int offset, int divisions)
	{
		if (arr.Length % divisions != 0) {
			Debug.LogError ("SlotManager: Array length isn't multiple of Division Count.");
			return null;
		}
		T[] sub = new T[arr.Length / 2];
		for (int i = 0; i < sub.Length; i++) {
			sub [i] = arr [i + offset * arr.Length / 2];
		}
		return sub; 
	}

	#endregion

	#region Spawn and Setup

	//    [ClientRpc]
	//    private void RpcRegisterSlot(int index, NetworkInstanceId ni)
	//    {
	//        _playerSlots[index] = ni;//NetworkUtility.FindLocalComponent<Slot>(ni);
	//        Debug.Log("_playerSlots[" + index + "]: " + _playerSlots[index]);
	//    }

	public void RegisterSlot (int idx, SlotType type, NetworkInstanceId slotNetId)
	{
		int count = 0;
		//Debug.Log("SlotManager: Registering " + type + "[" + idx + "] = [NetId:" + netId + "]");
		switch (type) {
		case SlotType.Slot:
			_playerSlots [idx] = slotNetId;
			count++;
            //Debug.Log("SlotManager: Registered [" + _playerSlots[idx] + "]");
			break;
		case SlotType.Row:
			_effectRowSlots [idx] = slotNetId;
            //Debug.Log("SlotManager: Registered [" + _playerRows[idx] + "]");
			count++;
			break;
		case SlotType.Column:
			_eventColumnSlots [idx] = slotNetId;
            //Debug.Log("SlotManager: Registered [" + _playerColumns[idx] + "]");
			count++;
			break;
		case SlotType.Deck:
			_playerDecks [idx] = slotNetId;
            //Debug.Log("SlotManager: Registered [" + _playerDecks[idx] + "]");
			count++;
			break;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}
	}

	[ServerCallback]
	private void SrvSpawnSlots ()
	{
		if (!isServer)
			return;
        
		if (_effectRowSlots.Count () == 0 || _eventColumnSlots.Count () == 0) {
			Debug.LogError ("SlotManager: Must be called after SpawnRows() and SpawnColumns()");
			return;
		}

		GameObject newObj;
		Slot newSlot;
		int slotIdx;
		for (int c = 0; c < _EventColumns; c++) {
			for (int r = 0; r < _EffectRows; r++) {
				// Umbra
				slotIdx = (_EffectRows * c + r);
				newObj = (GameObject)Instantiate (
					original: _SlotMarkerPrefab, 
					position: new Vector3 (c * _Spacing, 0, r * _Spacing) + transform.position, 
					rotation: _SlotMarkerPrefab.transform.rotation);
				newSlot = newObj.GetComponent<Slot> ();
				newSlot.AssignStructure (
					slotIndex: slotIdx, 
					slotType: SlotType.Slot, 
					ownerRole: PlayerRole.None, 
					rowSlot: _effectRowSlots [r], 
					columnSlot: _eventColumnSlots [c]
				);
				newSlot.AssignNetParent (this.netId);
				newSlot.name = string.Format ("Umbra Slot {0:00} ({1:0}, {2:0})", slotIdx, c, r);
				NetworkServer.Spawn (newObj);

				// Lux
				slotIdx = _EventColumns * _EffectRows + (_EffectRows * c + r);
				newObj = (GameObject)Instantiate (
					original: _SlotMarkerPrefab, 
					position: new Vector3 (c * _Spacing, 0, -(r + 1) * _Spacing) + transform.position, 
					rotation: _SlotMarkerPrefab.transform.rotation);
				newSlot = newObj.GetComponent<Slot> ();
				newSlot.AssignStructure (
					slotIndex: slotIdx, 
					slotType: SlotType.Slot, 
					ownerRole: PlayerRole.None, 
					rowSlot: _effectRowSlots [r + _EffectRows], 
					columnSlot: _eventColumnSlots [c + _EventColumns]
				);
				newSlot.AssignNetParent (this.netId);
				newSlot.name = string.Format ("Lux Slot {0:00} ({1:0}, {2:0})", slotIdx, (c + _EventColumns), (r + _EffectRows));
				NetworkServer.Spawn (newObj);
			}
		}
	}

	//    [ClientRpc]
	//    private void RpcRegisterRowSlot(int index, NetworkInstanceId ni)
	//    {
	//        _playerRows[index] = ni;//NetworkUtility.FindLocalComponent<Slot>(ni);
	//        Debug.Log("_playerRows[" + index + "]: " + _playerRows[index]);
	//    }

	[ServerCallback]
	private void SrvSpawnColumns ()
	{
		if (!isServer)
			return;
        
		GameObject newObj;
		Slot newSlot;

		int slotIdx = 0;
		for (int c = 0; c < _EventColumns; c++) {
			// Umbra
			slotIdx = c;
			newObj = (GameObject)Instantiate (
				original: _RowMarkerPrefab, 
				position: new Vector3 (c * _Spacing, 0, _EffectRows * _Spacing) + transform.position, 
				rotation: _RowMarkerPrefab.transform.rotation);
			newSlot = newObj.GetComponent<Slot> ();
			newSlot.AssignStructure (
				slotIndex: slotIdx, 
				slotType: SlotType.Column, 
				ownerRole: PlayerRole.Umbra, 
				rowSlot: NetworkInstanceId.Invalid, 
				columnSlot: NetworkInstanceId.Invalid
			);
			newSlot.AssignNetParent (this.netId);

			newSlot.name = string.Format ("Umbra Col {0:00}", slotIdx);
			NetworkServer.Spawn (newObj);

			// Lux
			slotIdx = _EventColumns + c;
			newObj = (GameObject)Instantiate (
				original: _RowMarkerPrefab, 
				position: new Vector3 (c * _Spacing, 0, -(_EffectRows + 1) * _Spacing) + transform.position, 
				rotation: _RowMarkerPrefab.transform.rotation);
			newSlot = newObj.GetComponent<Slot> ();
			newSlot.AssignStructure (
				slotIndex: slotIdx, 
				slotType: SlotType.Column, 
				ownerRole: PlayerRole.Lux, 
				rowSlot: NetworkInstanceId.Invalid, 
				columnSlot: NetworkInstanceId.Invalid
			);
			newSlot.AssignNetParent (this.netId);
			newSlot.name = string.Format ("Lux Col {0:00}", slotIdx);
			NetworkServer.Spawn (newObj);
		}
	}

	//    [ClientRpc]
	//    private void RpcRegisterColumnSlot(int index, NetworkInstanceId ni)
	//    {
	//        _playerColumns[index] = ni;//NetworkUtility.FindLocalComponent<Slot>(ni);
	//        Debug.Log("_playerColumns[" + index + "]: " + _playerColumns[index]);
	//    }

	[ServerCallback]
	private void SrvSpawnRows ()
	{
		if (!isServer)
			return;
        
		GameObject newObj;
		Slot newSlot;

		int slotIdx = 0;
		for (int r = 0; r < _EffectRows; r++) {
			slotIdx = r;
			newObj = (GameObject)Instantiate (
				original: _ColumnMarkerPrefab, 
				position: new Vector3 (_EventColumns * _Spacing, 0, r * _Spacing) + transform.position, 
				rotation: _ColumnMarkerPrefab.transform.rotation);
			newSlot = newObj.GetComponent<Slot> ();
			newSlot.AssignStructure (
				slotIndex: slotIdx, 
				slotType: SlotType.Row, 
				ownerRole: PlayerRole.Umbra, 
				rowSlot: NetworkInstanceId.Invalid, 
				columnSlot: NetworkInstanceId.Invalid
			);
			newSlot.AssignNetParent (this.netId);
			newSlot.name = string.Format ("Umbra Row {0:00}", slotIdx);
			NetworkServer.Spawn (newObj);

			slotIdx = _EffectRows + r;
			newObj = (GameObject)Instantiate (
				original: _ColumnMarkerPrefab, 
				position: new Vector3 (_EventColumns * _Spacing, 0, -(r + 1) * _Spacing) + transform.position, 
				rotation: _ColumnMarkerPrefab.transform.rotation);
			newSlot = newObj.GetComponent<Slot> ();
			newSlot.AssignStructure (
				slotIndex: slotIdx, 
				slotType: SlotType.Row, 
				ownerRole: PlayerRole.Lux, 
				rowSlot: NetworkInstanceId.Invalid, 
				columnSlot: NetworkInstanceId.Invalid
			);
			newSlot.AssignNetParent (this.netId);
			newSlot.name = string.Format ("Lux Row {0:00}", slotIdx);
			NetworkServer.Spawn (newObj);
		}
	}

	//    [ClientRpc]
	//    private void RpcRegisterDeckSlot(int index, NetworkInstanceId ni)
	//    {
	//        _playerDecks[index] = ni;//NetworkUtility.FindLocalComponent<Slot>(ni);
	//        //Debug.Log("_playerDecks[" + index + "]: " + _playerDecks[index]);
	//    }

	[ServerCallback]
	private void SrvSpawnDecks ()
	{
		if (!isServer)
			return;
        
		GameObject newObj;
		Slot newSlot;

		int slotIdx = 0;
		for (int r = 0; r < _DeckRows; r++) {
			for (int c = 0; c < _EventColumns; c++) {
				// Player 1
				slotIdx = (_EventColumns * r + c);
				newObj = (GameObject)Instantiate (
					original: _DeckMarkerPrefab, 
					position: new Vector3 (c * _Spacing, 0, (_EffectRows + 1 + r) * _Spacing) + transform.position, 
					rotation: _ColumnMarkerPrefab.transform.rotation);
				newSlot = newObj.GetComponent<Slot> ();
				newSlot.AssignStructure (
					slotIndex: slotIdx, 
					slotType: SlotType.Deck, 
					ownerRole: PlayerRole.Umbra, 
					rowSlot: NetworkInstanceId.Invalid, 
					columnSlot: NetworkInstanceId.Invalid
				);
				newSlot.AssignNetParent (this.netId);
				newSlot.name = "Umbra Deck " + slotIdx;
				NetworkServer.Spawn (newObj);

				// Player 2
				slotIdx = (_DeckRows * _EventColumns) + (_EventColumns * r + c);
				newObj = (GameObject)Instantiate (
					original: _DeckMarkerPrefab, 
					position: new Vector3 (c * _Spacing, 0, -(_EffectRows + 2 + r) * _Spacing) + transform.position, 
					rotation: _ColumnMarkerPrefab.transform.rotation);
				newSlot = newObj.GetComponent<Slot> ();
				newSlot.AssignStructure (
					slotIndex: slotIdx, 
					slotType: SlotType.Deck, 
					ownerRole: PlayerRole.Lux, 
					rowSlot: NetworkInstanceId.Invalid, 
					columnSlot: NetworkInstanceId.Invalid
				);
				newSlot.AssignNetParent (this.netId);
				newSlot.name = "Lux Deck " + slotIdx;
				NetworkServer.Spawn (newObj);
			}
		}
	}

	[ClientRpc]
	public void RpcAssignItemToSlot (NetworkInstanceId itemNetId, NetworkInstanceId slotNetId)
	{
		Debug.Log ("SlotManager: " + NetworkUtility.NetIdStr (this) + " RpcAssignItemToSlot(item: " + itemNetId + ", slot: " + slotNetId + ")");
		Slot localSlot = NetworkUtility.FindLocalComponent<Slot> (slotNetId);
		//Debug.Log("IsServer: " + isServer + " IsClient: " + isClient + " NI: " + ni);
		if (localSlot != null) {
			localSlot.CatchItem (itemNetId);
		}
	}

	[ServerCallback]
	public void SrvAddToDeck (PlayerRole role, Item newItem)
	{
		//bool success = false;

		if (newItem == null) {
			Debug.LogWarning ("SlotManager: Null item added to deck. Ignored.");
			return;
		}

		// deck determines role
		newItem.SrvSetRole (role);
		Slot slot;
		foreach (var slotId in _playerDecks) {
			slot = NetworkUtility.FindLocalComponent<Slot> (slotId);
			if (slot.GetItem () == null && slot.ownerRole == role) {
				//success = slot.CatchItem (newItem);
				slot.CatchItem (newItem.netId);
				//Debug.Log("Is Server " + isServer + ", ");
				RpcAssignItemToSlot (newItem.netId, slot.netId);
				return;
			}

		}
		Debug.LogError ("SlotManager: No deck slot availeble for item [NetId" + newItem.netId + "] (" + newItem._ownerRole + ")");
		return;
	}

	void InitializeFields ()
	{
		Debug.Log ("SlotManager: Initializing Fields");
		// clear old and init new arrays
		_playerSlots = new NetworkInstanceId[_EventColumns * _EffectRows * 2];
		_playerDecks = new NetworkInstanceId[_DeckRows * _EventColumns * 2];
		_effectRowSlots = new NetworkInstanceId[_EffectRows * 2];
		_eventColumnSlots = new NetworkInstanceId[_EventColumns * 2];
		InitArr<NetworkInstanceId> (_playerSlots, NetworkInstanceId.Invalid);
		InitArr<NetworkInstanceId> (_playerDecks, NetworkInstanceId.Invalid);
		InitArr<NetworkInstanceId> (_eventColumnSlots, NetworkInstanceId.Invalid);
		InitArr<NetworkInstanceId> (_effectRowSlots, NetworkInstanceId.Invalid);
	}

	public static void InitArr<T> (T[] arr, T value)
	{
		for (int i = 0; i < arr.Length; i++) {
			arr [i] = value;
		}
	}

	[ServerCallback]
	public void SrvClear ()
	{
		#if LOG
		Debug.Log ("SlotManager: Destroying Slots");
		#endif
//        var childObjects = transform.Cast<Transform>().ToList();
//        if (Application.isEditor) {
//            foreach (Transform child in childObjects) {
//                DestroyImmediate(child.gameObject);
//            }
//        } else {
//            foreach (Transform child in childObjects) {
//                Destroy(child.gameObject);
//            }
//        }

		Slot[] objs = FindObjectsOfType<Slot> ();
		for (int i = 0; i < objs.Count (); i++) {
			if (Application.isEditor)
				DestroyImmediate (objs [i].gameObject);
			else
				Destroy (objs [i].gameObject);
		}

	}

	[ServerCallback]
	public void SrvSpawn ()
	{
		if (isServer) {
			#if LOG
			Debug.Log ("SlotManager: (Server) Setting up slots.");
			#endif
			//InitializeFields();
			SrvSpawnRows ();
			SrvSpawnColumns ();
			SrvSpawnSlots ();
			SrvSpawnDecks ();
		} else {
			//InitializeFields();
		}

	}

	protected void Refresh ()
	{
		throw new UnityException ("SlotManager: Not implemented");
	}

	#endregion
}
