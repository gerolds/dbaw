﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class IntroStory : Story
{
    public IntroStory()
    {
        fade = .5f;
        title = "Intro";
        scenes.Add(new StoryScene(false, 2f, "Willkommen in der Besten Aller Welten"));
        scenes.Add(new StoryScene(false, 2f, "Ich bin [Akna]"));
        scenes.Add(new StoryScene(false, 3f, "Deine Assistentin für alle Fragen zu dieser Welt."));
        scenes.Add(new StoryScene(false, 10f, "Bitte Verbinde dich mit deinem Interface [^]"));
        Debug.Log("IntroStory: " + scenes.Count + " scenes initialized");
    }
}

public class TutorialStory : Story
{
    public TutorialStory()
    {
        fade = .5f;
        title = "Tutorial";
        scenes.Add(new StoryScene(false, 0f, "[Tutorial]"));
        scenes.Add(new StoryScene(false, 0f, "Disclaimer: Please note that this exercise takes place in real time.\n\nTherfore you response to my instruction must be prompt, lest it may have consequences that may invalidate the results of this experiment."));
        scenes.Add(new StoryScene(false, 0f, "Enjoy your stay, nevertheless."));
        scenes.Add(new StoryScene(false, 0f, "We will now begin your training."));
        scenes.Add(new StoryScene(false, 0f, ".", () =>
                {
                    SlotManager.singleton.ResetAllFoundations();
                }));
        scenes.Add(new StoryScene(false, 0f, "."));
        scenes.Add(new StoryScene(false, 0f, "."));
        scenes.Add(new StoryScene(false, 0f, "Observe, this is [Calakmul]. An ancient city located in what is known today as Yucatan."));
        scenes.Add(new StoryScene(false, 0f, "In this simulation you will make choices based on [Yich'aak K'ahk']'s rule from CE 686 to 695."));
        scenes.Add(new StoryScene(false, 0f, "Your primary goal ist to establish a solid economic foundation to support the growing population in your city state."));
        scenes.Add(new StoryScene(false, 0f, "[..]"));
        scenes.Add(new StoryScene(false, 0f, "And so it begins."));
        scenes.Add(new StoryScene(false, 0f, "", () =>
                {
                    GameManager.singleton.ProcessEvent(GEvent.Continue);
                }));
        Debug.Log("TutorialStory: " + scenes.Count + " scenes initialized");
    }
}

public class FirstRoundStory : Story
{
    public FirstRoundStory()
    {
        fade = .5f;
        title = "1st Round";
        scenes.Add(new StoryScene(false, 1f, "Resetting foundation...", () =>
                {
                    SlotManager.singleton.QuicklyResetAllFoundations();
                }));
        scenes.Add(new StoryScene(false, 0f, "Breaking ground...", () =>
                {
                    SlotManager.singleton.FoundCenterTile();
                }));
        Debug.Log("FirstRoundStory: " + scenes.Count + " scenes initialized");
    }
}

public class SecondRoundStory : Story
{
    public SecondRoundStory()
    {
        fade = .5f;
        title = "2nd Round";
        scenes.Add(new StoryScene(false, 2f, "[Second Round]"));
        Debug.Log("SecondRoundStory: " + scenes.Count + " scenes initialized");
    }
}

public class ThirdRoundStory : Story
{
    public ThirdRoundStory()
    {
        fade = .5f;
        title = "3rd Round";
        scenes.Add(new StoryScene(false, 2f, "[Third Round]"));
        Debug.Log("ThirdRoundStory: " + scenes.Count + " scenes initialized");
    }
}

public class FirstIntermissionStory : Story
{
    public FirstIntermissionStory()
    {
        fade = .5f;
        title = "1st Intermission";
        scenes.Add(new StoryScene(false, 2f, "[First Intermission]"));
        scenes.Add(new StoryScene(false, 0f, "", () =>
                {
                    VideoInputManager.singleton.ShowGhost();
                }));
        scenes.Add(new StoryScene(false, 2f, "[...]"));
        scenes.Add(new StoryScene(false, 0f, "", () =>
                {
                    GameManager.singleton.ProcessEvent(GEvent.Continue);
                }));
        Debug.Log("FirstIntermissionStory: " + scenes.Count + " scenes initialized");
    }
}

public class SecondIntermissionStory : Story
{
    public SecondIntermissionStory()
    {
        fade = .5f;
        title = "2nd Intermission";
        scenes.Add(new StoryScene(false, 2f, "[Second Intermission]"));
        scenes.Add(new StoryScene(false, 2f, "[...]"));
        scenes.Add(new StoryScene(false, 0f, "", () =>
                {
                    GameManager.singleton.ProcessEvent(GEvent.Continue);
                }));
        Debug.Log("SecondIntermissionStory: " + scenes.Count + " scenes initialized");
    }
}

public class ThirdIntermissionStory : Story
{
    public ThirdIntermissionStory()
    {
        fade = .5f;
        title = "3rd Intermission";
        scenes.Add(new StoryScene(false, 2f, "[Third Intermission]"));
        scenes.Add(new StoryScene(false, 2f, "[Completing game]"));
        scenes.Add(new StoryScene(false, 0f, "", () =>
                {
                    GameManager.singleton.ProcessEvent(GEvent.Complete);
                }));
        Debug.Log("ThirdIntermissionStory: " + scenes.Count + " scenes initialized");
    }
}

public class ConclusionStory : Story
{
    public ConclusionStory()
    {
        fade = .5f;
        title = "Conclusion";
        scenes.Add(new StoryScene(false, 5f, "[Conclusion]"));
        scenes.Add(new StoryScene(false, 2f, "[...]"));
        Debug.Log("ConclusionStory: " + scenes.Count + " scenes initialized");
    }
}

public class Stories : MonoBehaviour
{
    public UnityEngine.UI.Text _target;

    public StoryPlayer pIntro;
    public StoryPlayer pTutorial;

    public StoryPlayer pFirstInter;
    public StoryPlayer pSecondInter;
    public StoryPlayer pThirdInter;

    public StoryPlayer pFirstRound;
    public StoryPlayer pSecondRound;
    public StoryPlayer pThirdRound;

    public StoryPlayer[] players;
    string stat = "";

    string Status
    {
        get
        {
            stat = "";
            foreach (var player in players)
            {
                stat += "\n[" + LeanTween.isTweening(player._targetUiText.gameObject) + "] --> " + player._storyToPlay.title + " (" + player._storyToPlay.scenes.Count + "), Condition: [" + player._count + ":" + player._runConditionCount + ":" + player._runConditionState + "]";
            }
            return stat;
        }
    }

    void Start()
    {
        pIntro = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pIntro.Consturct(
            target: _target,
            story: new IntroStory(),
            runOnState: GState.Offline,
            runOnCount: 0,
            resetCount: false
        );

        pTutorial = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pTutorial.Consturct(
            target: _target,
            story: new TutorialStory(),
            runOnState: GState.Intermission,
            runOnCount: 0,
            resetCount: false
        );

        pFirstInter = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pFirstInter.Consturct(
            target: _target,
            story: new FirstIntermissionStory(),
            runOnState: GState.Intermission,
            runOnCount: 1,
            resetCount: false
        );

        pSecondInter = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pSecondInter.Consturct(
            target: _target,
            story: new SecondIntermissionStory(),
            runOnState: GState.Intermission,
            runOnCount: 2,
            resetCount: false
        );

        pThirdInter = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pThirdInter.Consturct(
            target: _target,
            story: new ThirdIntermissionStory(),
            runOnState: GState.Intermission,
            runOnCount: 3,
            resetCount: false
        );

        pFirstRound = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pFirstRound.Consturct(
            target: _target,
            story: new FirstRoundStory(),
            runOnState: GState.Running,
            runOnCount: 0,
            resetCount: false
        );

        pSecondRound = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pSecondRound.Consturct(
            target: _target,
            story: new SecondRoundStory(),
            runOnState: GState.Running,
            runOnCount: 1,
            resetCount: false
        );

        pThirdRound = gameObject.AddComponent<StoryPlayer>() as StoryPlayer;
        pThirdRound.Consturct(
            target: _target,
            story: new ThirdRoundStory(),
            runOnState: GState.Running,
            runOnCount: 2,
            resetCount: false
        );
        players = GetComponents<StoryPlayer>();
    }
}

