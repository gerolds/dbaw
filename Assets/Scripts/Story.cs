﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public struct StoryScene
{
	public StoryScene (bool b, float d, string t, Action a = null, Predicate<int> waitUntilTrue = null)
	{
		skip = b;
		text = t;
		duration = d;
		action = a;
		wait = waitUntilTrue;
	}

	public bool skip;
	public string text;
	public float duration;
	public Action action;
	public Predicate<int> wait;
}

public abstract class Story
{
	public List<StoryScene> scenes = new List<StoryScene> ();
	protected float fade = 1f;
	public bool running = false;
	public string title = "[no title]";

	public virtual void RunStory (Text target)
	{
		Debug.Log ("Story: Running with " + scenes.Count + " scenes.");
		LeanTween.cancel (target.gameObject);
		target.enabled = true;
		running = true;
		float delay = 0;
		int j = 0;
		for (int i = 0; i < scenes.Count; i++) {

			if (scenes [i].skip)
				continue;

//            if (scenes[i].predicate != null) {
//                yield return new WaitUntil(scenes[i].predicate);
//            }
            
			// change text
			LeanTween.delayedCall (
				target.gameObject,
				delay + fade,
				() => {
					Debug.Log ("Story: text index [" + j + "]");
					target.text = scenes [j].text;
					if (scenes [j].action != null) {
						scenes [j].action.Invoke ();
					}
					j++;
				}
			);

			// fade out
			LeanTween.alphaText (
				target.rectTransform,
				0.0f,
				fade).setDelay (delay);

			// fade in
			LeanTween.alphaText (
				target.rectTransform, 
				1.0f, 
				fade).setDelay (delay + fade);

			// calculate delay for next scene
			delay += fade * 2 + scenes [i].duration;
		}
		// fade in
		LeanTween.alpha (
			target.rectTransform, 
			0.0f, 
			fade).setDelay (delay);
	}

	public virtual void StopStory (Text target)
	{
		if (LeanTween.isTweening (target.gameObject)) {
			Debug.Log ("Story: cancelled.");
			LeanTween.cancel (target.gameObject);
		}
		target.enabled = false;

	}
}