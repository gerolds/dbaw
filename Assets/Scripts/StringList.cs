﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;

[System.Serializable]
public class StringListItem
{
    public bool boolValue;
    public string stringvalue;
}

public class StringList : MonoBehaviour
{
    [HideInInspector] // we do this beacause we call base.OnInspectorGUI(); And we don't want that to draw the list aswell.
    public List<StringListItem> data;
}