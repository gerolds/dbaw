﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class TextHandler : MonoBehaviour
{

	private Vector3 _origScale;
	private Text _target;
	public float _Duration = 2.0f;
	private float _attack = .25f;
	private float _decay = .8f;
	private float _release = .25f;
	private float _sustain = 2.0f;

	void Start ()
	{
		_target = GetComponent<Text> ();
		_origScale = _target.rectTransform.localScale;
		_target.color = new Color (
			_target.color.r,
			_target.color.g,
			_target.color.b,
			0
		);
		ShowMessage ("Test");
	}

	public void ShowMessage (string msg)
	{
		Color visible = new Color (
			                _target.color.r,
			                _target.color.g,
			                _target.color.b,
			                1.0f
		                );
		_target.text = msg;

//        Color invisible = new Color (
//            _target.color.r,
//            _target.color.g,
//            _target.color.b,
//            0
//        );
		_target.rectTransform.localScale = Vector3.zero;
		LeanTween.cancel (gameObject);
		LeanTween.textAlpha (_target.rectTransform, 1f, _attack);
		LeanTween.textAlpha (_target.rectTransform, _decay, _sustain).setDelay (_attack);
		LeanTween.textAlpha (_target.rectTransform, 0f, _release).setDelay (_attack + _sustain);
		LeanTween.scale (_target.rectTransform, _origScale, _attack);
		LeanTween.scale (_target.rectTransform, _origScale * _decay, _sustain).setDelay (_attack);
		LeanTween.scale (_target.rectTransform, Vector3.zero, _release).setDelay (_attack + _sustain);
	}
}
