﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TimerDisplay : LocalGameBehaviour
{

	public Text targetText;
	public Image targetImage;
	private float barProgress;
	private float turnStart;

	protected override void StartTurnHandler (int turn)
	{
		// reset bar
		Debug.Log ("TimerDisplay: Start turn");
		ResetBar ();
	}

	void Update ()
	{
		if (GameManager.singleton.CurrentState != GState.Running)
			return;
		
		if (NetGameManager.singleton != null) {
			int time = NetGameManager.singleton.GameTime;
			targetText.text = String.Format ("0:{0:00}", time);

			barProgress = 1f - Mathf.Clamp01 ((Time.time - turnStart) / (float)NetGameManager.singleton.TurnTimeout);
			targetImage.rectTransform.sizeDelta = new Vector2 (
				barProgress, 
				targetImage.rectTransform.sizeDelta.y
			);
		}
	}

	void ResetBar ()
	{
		turnStart = Time.time;
		Debug.Log ("TimerDisplay: Reset bar");
	}
}
