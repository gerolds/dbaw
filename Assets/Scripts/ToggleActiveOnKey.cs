﻿using UnityEngine;
using System.Collections;

public class ToggleActiveOnKey : MonoBehaviour
{
	public KeyCode _Key = KeyCode.Caret;

	public bool _Visible = true;

	void Start ()
	{
		foreach (Transform child in transform) {
			child.gameObject.SetActive (_Visible);
		}
	}

	void Update ()
	{
		if (Input.GetKeyDown (_Key)) {
			foreach (Transform child in transform) {
				child.gameObject.SetActive (!_Visible);
				_Visible = !_Visible;
			}
		}
	}
}
