﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class ToggleListEntryUI : MonoBehaviour
{
	public VideoInputManager VideoInputManager;
	public Text Label;
	public string Value;
	private Button _button;

	void Start ()
	{
		_button = GetComponent<Button> ();
		_button.onClick.AddListener (OnClick);
	}

	void OnClick ()
	{
		VideoInputManager.SetDevice (Value);
	}

	void Update ()
	{
		Label.text = Value;
	}
}
