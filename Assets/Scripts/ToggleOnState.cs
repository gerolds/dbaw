﻿using UnityEngine;
using System.Collections;

public class ToggleOnState : MonoBehaviour
{

    [EnumFlagAttribute]
    public GState _ActiveStates = GState.Running;

    private bool _active = true;

    void Update()
    {
        if (GameManager.singleton == null) {
            Debug.Log("No GameManager found");
            return;
        }
        
        if ((_ActiveStates & GameManager.singleton.CurrentState) != 0) {
            foreach (Transform child in transform) {
                child.gameObject.SetActive(true);
            }
        } else {
            foreach (Transform child in transform) {
                child.gameObject.SetActive(false);
            }
        }
    }
}
