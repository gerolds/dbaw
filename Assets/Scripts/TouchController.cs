﻿#if DEVELOPMENT_BUILD
//#define LOG
#endif

using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour
{

	public LayerMask TriggerMask;

	void OnTriggerStay (Collider hit)
	{
		if (hit.gameObject.CompareTag ("Touchable")) {
			#if LOG
			Debug.Log ("Touch stay");
			#endif

			hit.SendMessage ("OnTouchStay", gameObject, SendMessageOptions.DontRequireReceiver);
		}
	}

	void OnTriggerEnter (Collider hit)
	{
        
		if (hit.gameObject.CompareTag ("Touchable")) {
			#if LOG
			Debug.Log ("Touch enter");
			#endif
			hit.SendMessage ("OnTouchEnter", gameObject, SendMessageOptions.DontRequireReceiver);
		}
	}

	void OnTriggerExit (Collider hit)
	{
        
		if (hit.gameObject.CompareTag ("Touchable")) {
			#if LOG
			Debug.Log ("Touch exit");
			#endif
			hit.SendMessage ("OnTouchExit", gameObject, SendMessageOptions.DontRequireReceiver);
		}
	}
}
