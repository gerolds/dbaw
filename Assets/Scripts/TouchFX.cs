﻿using UnityEngine;

public abstract class TouchFX : MonoBehaviour
{
    public abstract void OnActiveExit(object o);

    public abstract void OnActiveStay(object o);

    public abstract void OnActiveEnter(object o);

	public abstract Transform Anchor {
		get;
	}
}

