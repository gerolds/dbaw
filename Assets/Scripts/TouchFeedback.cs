﻿using UnityEngine;
using System.Collections;

public class TouchFeedback : MonoBehaviour
{
	private Component[] fxComponents;

	void Start ()
	{
		fxComponents = GetComponentsInChildren (typeof(TouchFX));
	}

	public void Refresh ()
	{
		fxComponents = GetComponentsInChildren (typeof(TouchFX));
	}

	public void OnTouchEnter (object o = null)
	{
		if (fxComponents != null) {
			foreach (TouchFX fx in fxComponents) {
				fx.OnActiveEnter (null);
			}
		}
	}

	public void OnTouchExit (object o = null)
	{
		if (fxComponents != null) {
			foreach (TouchFX fx in fxComponents) {
				fx.OnActiveExit (null);
			}
		}
	}


	public void OnTouchStay (object o = null)
	{
		if (fxComponents != null) {
			foreach (TouchFX fx in fxComponents) {
				fx.OnActiveStay (null);
			}
		}
	}
}
