﻿using UnityEngine;
using System.Collections;

public class TransformSnapshot : MonoBehaviour
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
}
