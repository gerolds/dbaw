﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TurnDisplay : MonoBehaviour
{
    public Text targetText;
    //public Image targetImage;
    public GameManager _GameManager;

    void Update()
    {
        if (NetGameManager.singleton != null) {
            targetText.text = String.Format("{0}", NetGameManager.singleton.GameTurn);
        }
    }
}
