﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Reflection;

public class UIReader : MonoBehaviour
{

    public GameObject _Source;
    public string _Component;
    public string _Field;
    public Text _Target;

    void Update()
    {
        if (_Source == null || _Target == null || _Component.Length == 0)
            return;

        Component c = _Source.GetComponent(_Component);
        if (c == null)
        {
            _Target.text = "Invalid Component";
            return;
        }

        PropertyInfo p = null;
        FieldInfo f = c.GetType().GetField(_Field, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
            p = c.GetType().GetProperty(_Field, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        
        if (f == null && p == null)
        {
            _Target.text = "Invalid Field <" + _Field + ">";
            return;
        }

        if (p != null)
        {
            _Target.text = p.Name + ": " + p.GetValue(c, null).ToString();
        }
        else if (f != null)
        {
            _Target.text = f.Name + ": " + f.GetValue(c).ToString();
        }
    }
}
