﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VideoInputManager : MonoBehaviour
{
    private WebCamDevice[] _devices;
    private string _deviceName;
    public ToggleGroup _ToggleGroup;
    public ToggleListEntryUI _EntryUIPrefab;
    private WebCamTexture _webcam;
    public RawImage _PreviewComponent;
    public MeshRenderer _DisplacementRenderer;
    public ParticleCloud _ParticleCloud;
    public BlockMesh _BlockMesh;

    public void HideGhost()
    {
        Debug.Log("VideoInputManager: Hide Ghost");
        _DisplacementRenderer.gameObject.SetActive(false);
    }

    public void ShowGhost()
    {
        Debug.Log("VideoInputManager: Show Ghost");
        _DisplacementRenderer.gameObject.SetActive(true);
    }

    #region SINGLETON

    public static VideoInputManager singleton = null;

    void Awake()
    {
        // Singleton
        if (singleton == null)
        {
            singleton = this;
        }
        else if (singleton != this)
        {
            Destroy(gameObject);    
        }
    }

    #endregion

    public WebCamDevice[] Devices
    {
        get
        {
            return _devices;
        }
    }

    public string DeviceName
    {
        get
        {
            return _deviceName;
        }
        set
        {
            _deviceName = value;
        }
    }

    void Update()
    {
        //if (_webcam != null)
        //Debug.Log(_webcam.width + " " + _webcam.height);
    }

    public void Stop()
    {
        _webcam.Stop();
    }

    void Start()
    {
        _devices = WebCamTexture.devices;
        Debug.Log(_devices.Length + " WebcamDevices found");
        for (int i = 0; i < _devices.Length; i++)
        {
            Debug.Log(_devices[i].name);
            _EntryUIPrefab.Value = _devices[i].name;
            _EntryUIPrefab.VideoInputManager = this;
            Instantiate(_EntryUIPrefab, _ToggleGroup.transform);
        }
    }

    public void SetDevice(string name)
    {
        Debug.Log("Video Input Device set to [" + name + "]");
        _deviceName = name;
        _webcam = new WebCamTexture(_deviceName);
        _webcam.Play();
        _webcam.requestedFPS = 30;
        _webcam.requestedHeight = 160;
        _webcam.requestedWidth = 90;
        _PreviewComponent.texture = _webcam;
        _webcam.Play();
        _webcam.wrapMode = TextureWrapMode.Repeat;
        //_DisplacementRenderer.material.SetTexture("_MainTex", null);
        _DisplacementRenderer.material.SetTexture("_DispTex", _webcam);
        _ParticleCloud._Data = _webcam;
        _BlockMesh._Data = _webcam;

    }
}
