﻿Shader "Tessellation Sample" {
        Properties {
            _EdgeLength ("Edge length", Range(2,50)) = 15
            _MainTex ("Color (RGB) Alpha (A)", 2D) = "white" {}
            _DispTex ("Disp Texture", 2D) = "gray" {}
            _NormalMap ("Normalmap", 2D) = "bump" {}
            _Displacement ("Displacement", Range(0, 1.0)) = 0.3
            _HeightmapStrength("Height Map Strength", Range(0, 256.0)) = 1.0
            _Color ("Color", color) = (1,1,1,0)
            _SpecColor ("Spec color", color) = (0.5,0.5,0.5,0.5)
			_MapDimX ("Map Width", Float) = 1920
			_MapDimY ("Map Height", Float) = 1080
        }
        SubShader {
            Tags { "Queue"="Transparent" "RenderType"="Transparent" }
            LOD 300
            
            CGPROGRAM
            #pragma surface surf BlinnPhong addshadow fullforwardshadows vertex:disp tessellate:tessEdge nolightmap alpha
            #pragma target 4.6
            #include "Tessellation.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
            };

            float _EdgeLength;

            float4 tessEdge (appdata v0, appdata v1, appdata v2)
            {
                return UnityEdgeLengthBasedTess (v0.vertex, v1.vertex, v2.vertex, _EdgeLength);
            }

            sampler2D _DispTex;
            float _Displacement;
            float _HeightmapStrength, _MapDimX, _MapDimY;
            sampler2D _MainTex;
            sampler2D _NormalMap;
            fixed4 _Color;

            void disp (inout appdata v)
            {
                float d = tex2Dlod(_DispTex, float4(v.texcoord.xy,0,0)).r * _Displacement;
                v.vertex.xyz += v.normal * d;
            }

            struct Input {
                float2 uv_MainTex;
            };

            void surf (Input IN, inout SurfaceOutput o) {
                half4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
                o.Albedo = c.rgb;
                o.Alpha = c.a;
//                if (tex2D(_DispTex, IN.uv_MainTex).x < 0.5)
//                	o.Alpha = 0.0;
//                else 
//                	o.Alpha = 1.0;

                o.Specular = 0.5;
                o.Gloss = 0.5;

                // Normals
                float3 normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
 
				float me = tex2D(_DispTex, IN.uv_MainTex).x;
				float n = tex2D(_DispTex,float2(IN.uv_MainTex.x,IN.uv_MainTex.y+1.0/_MapDimY)).x;
				float s = tex2D(_DispTex,float2(IN.uv_MainTex.x,IN.uv_MainTex.y-1.0/_MapDimY)).x;
				float e = tex2D(_DispTex,float2(IN.uv_MainTex.x-1.0/_MapDimX,IN.uv_MainTex.y)).x;
				float w = tex2D(_DispTex,float2(IN.uv_MainTex.x+1.0/_MapDimX,IN.uv_MainTex.y)).x;
  
				float3 norm = normal;
				float3 temp = norm; //a temporary vector that is not parallel to norm
				if(norm.x==1)
					temp.y+=0.5;
				else
					temp.x+=0.5;
 
				//form a basis with norm being one of the axes:
				float3 perp1 = normalize(cross(norm,temp));
				float3 perp2 = normalize(cross(norm,perp1));
 
				//use the basis to move the normal in its own space by the offset
				float3 normalOffset = -_HeightmapStrength * ( ( (n - me) - (s - me) ) * perp1 + ( ( e - me ) - ( w - me ) ) * perp2 );
				norm += normalOffset;
				norm = normalize(norm);
 
				o.Normal = norm;
            }
            ENDCG
        }
        FallBack "Diffuse"
    }