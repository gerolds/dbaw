﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class StoryPlayer : MonoBehaviour
{
	public Text _targetUiText;
	public Story _storyToPlay;
	public GState _runConditionState;
	public int _runConditionCount = 0;
	public bool _resetCountAfterRun = false;
	public int _count = 0;
	public bool _isInitialized = false;

	public void OnEnable ()
	{
		GameManager.singleton.OnEnterState.AddListener (Run);
		GameManager.singleton.OnExitState.AddListener (StopRun);
	}

	public void OnDisable ()
	{
		GameManager.singleton.OnEnterState.RemoveListener (Run);
		GameManager.singleton.OnExitState.RemoveListener (StopRun);

	}

	public void Consturct (Text target, Story story, GState runOnState, int runOnCount, bool resetCount = false)
	{
		_targetUiText = target;
		_storyToPlay = story;
		_resetCountAfterRun = resetCount;
		_runConditionCount = runOnCount;
		_runConditionState = runOnState;
		_isInitialized = true;
	}

	public void Run (GState state)
	{
		if (!_isInitialized)
			throw new UnityException ("Story not initialized.");

		if (state.Equals (_runConditionState)) {
			if (_count == _runConditionCount) {
				#if LOG
				Debug.Log ("StoryPlayer: Starting " + _runConditionState + " == " + state);
				#endif
				_storyToPlay.RunStory (_targetUiText);
				if (_resetCountAfterRun)
					_count = 0;
				else
					_count++;
			} else {
				_count++;    
			}
		}
	}

	public void StopRun (GState s)
	{
		if (!_isInitialized)
			throw new UnityException ("Story not initialized.");

		//Debug.Log("StoryPlayer: Aborting");
		_storyToPlay.StopStory (_targetUiText);
	}
}