﻿#if DEVELOPMENT_BUILD
#define LOG
#endif

using UnityEngine;
using System.Collections;

public class TurnMessages : CustomNetworkBehaviour
{

    public TextHandler _target;

    override public void OnStartClient()
    {
        base.OnStartClient();
        NetGameManager.singleton.OnStartTurn.AddListener(TurnStart);
        NetGameManager.singleton.OnEndRound.AddListener(RoundEnd);
        NetGameManager.singleton.OnStartRound.AddListener(RoundStart);
    }

    void Start()
    {
//		NetGameManager.singleton.OnStartTurn.AddListener (YourTurn);
//		NetGameManager.singleton.OnEndRound.AddListener (RoundComplete);
    }

    void OnDisable()
    {
        NetGameManager.singleton.OnStartTurn.RemoveListener(TurnStart);
        NetGameManager.singleton.OnEndRound.RemoveListener(RoundEnd);
        NetGameManager.singleton.OnStartRound.RemoveListener(RoundStart);
    }

    void TurnStart(int t = 0)
    {
        if (PlayerManager.singleton.LocalRole == NetGameManager.singleton.GetTurnPlayer())
        {
            _target.ShowMessage(Lang.Tr("Ihr Zug"));
        }
        else
        {
            _target.ShowMessage(Lang.Tr("Gegenzug"));
        }
		
    }

    void RoundEnd(int r = 0)
    {
        _target.ShowMessage(Lang.Tr("Runde {0} beendet", r));
    }

    void RoundStart(int r = 0)
    {
        _target.ShowMessage(Lang.Tr("Runde {0} beginnt", r));
    }
}
