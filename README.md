# README

Die Beste Aller Welten, VR-Spiel.

# Projekt Synopsis

Martha und Kiko leben auf einer Müllhalde, auf die in regelmässigen Abständen Objekte aus einer anderen, vielleicht virtuellen Welt fallen. Kiko baut aus den Objekten lustige Dinge und spielt damit, zusammen mit ihrem Roboter Freund Meebo. 
Martha arbeitet als Schauspielerin. Sie nutzt KIPA, einen grösseren vierbeinigen Roboter mit einem grossen Display und einer Kamera um in regelmässigen Abständen zu arbeiten. Zwei Personen an einem anderen Ort spielen ein Spiel. Die eine versucht, abstrakte Objekte entlang einer Linie zu positionieren. Die andere versucht mit ihren Objekten durch die Linie zu brechen. Die Gewinnerin bekommt 5 Minuten zeit mit einer persönlichen Glücksagentin.

# Kontakt

gerolds@gmail.com

# Sneak Peek:

![Screenshot_20161001_095125.png](https://bitbucket.org/repo/9LxaGn/images/116044569-Screenshot_20161001_095125.png)